package net.secodo.besecurefirewall.config;

import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.action.FirewallActionsRepository;
import net.secodo.besecurefirewall.action.impl.AllowFirewallAction;
import net.secodo.besecurefirewall.action.impl.DenyFirewallAction;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.callback.FirewallRuleCallback;
import net.secodo.besecurefirewall.rule.matcher.impl.IpAddressMatcher;
import net.secodo.besecurefirewall.rule.matcher.impl.ServletPathMatcher;
import net.secodo.besecurefirewall.rule.matcher.pattern.impl.IpAddressPattern;
import net.secodo.besecurefirewall.testdata.callback.MatchesCounterCallback;
import org.junit.Test;
import net.secodo.besecurefirewall.rule.matcher.impl.UrlMatcher;
import net.secodo.besecurefirewall.rule.matcher.pattern.impl.StringPattern;
import java.lang.reflect.Field;
import java.util.Collections;

import static java.util.Arrays.asList;
import static java.util.Collections.EMPTY_LIST;
import static net.secodo.besecurefirewall.action.FirewallActionsRepository.Signatures.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;


public class ConfigReaderTest {
  @Test
  public void shouldReturnUninitializedConfigIfClassPathResourceDoesNotExist() throws Exception {
    // given
    final String notExistentFile = "this_does_not_exist.properties";

    // when
    final BeSecureFirewallConfig config = new ConfigReader().readConfigFromClassPathResource(notExistentFile);

    // then
    assertThat(config, notNullValue());
    assertThat(config.isDirty(), equalTo(true));


  }

  @Test
  public void shouldReturnInitializedConfigIfClassPathResourceExist() throws Exception {
    // given
    final String configFile = "besecure_empty_test.properties";

    // when
    final ConfigReader configReader = prepareConfigReader();


    final BeSecureFirewallConfig config = configReader.readConfigFromClassPathResource(configFile);

    // then
    assertThat(config, notNullValue());
    assertThat(config.isDirty(), equalTo(false));

  }

  @Test
  public void shouldReadAllRulesFromWellDefinedConfigFile() throws Exception {
    // given
    final String configFile = "besecure_complete_test.properties";
    final ConfigReader configReader = prepareConfigReader();

    // when
    final BeSecureFirewallConfig config = configReader.readConfigFromClassPathResource(configFile);

    // then
    final FirewallRuleCallback
      counterCallback = config.getCallbacksRepository().getByConfigSignature("COUNTER_CALLBACK"),
      noOpCallback = config.getCallbacksRepository().getByConfigSignature("NO_OP_CALLBACK");

    assertThat(counterCallback, notNullValue());
    assertThat(MatchesCounterCallback.class.isInstance(counterCallback), equalTo(true));

    assertThat(config, notNullValue());
    assertThat(config.isDirty(), equalTo(false));
    assertThat(config.getFirewallRules(),
      hasItem(
        new FirewallRule("drop0Char",
          new UrlMatcher(),
          Collections.<FirewallRuleCallback>emptyList(),
          new StringPattern(new String(new char[] { (char) 0 })),
          config.getActionsRepository().getBySignature(DROP))));
    assertThat(config.getFirewallRules(),
      hasItem(
        new FirewallRule("someRule2",
          new ServletPathMatcher(),
          asList(counterCallback),
          new StringPattern(new String("eviltext")),
          config.getActionsRepository().getBySignature(DENY))));
    assertThat(config.getFirewallRules(),
      hasItem(
        new FirewallRule("allowRule",
          new ServletPathMatcher(),
          Collections.<FirewallRuleCallback>emptyList(),
          new StringPattern(new String(new char[] { (char) 0 })),
          config.getActionsRepository().getBySignature(ALLOW))));
    assertThat(config.getFirewallRules(),
      hasItem(
        new FirewallRule("colonRule",
          new UrlMatcher(),
          asList(noOpCallback, counterCallback),
          new StringPattern("any:300"),
          config.getActionsRepository().getBySignature(ACKNOWLEDGE))));

  }

  @Test
  public void shouldReadWellDefinedRulesFromConfigFileWhileSkippingTheWronglyDefinedRules() throws Exception {
    // given
    final String configFile = "besecure_not_complete_test.properties";
    final ConfigReader configReader = prepareConfigReader();

    // when
    final BeSecureFirewallConfig config = configReader.readConfigFromClassPathResource(configFile);

    // then
    assertThat(config, notNullValue());
    assertThat(config.isDirty(), equalTo(false));
    assertThat(config.getFirewallRules().size(), equalTo(1));
    assertThat(config.getFirewallRules(),
      hasItem(
        new FirewallRule("correctRule",
          new IpAddressMatcher(),
          Collections.<FirewallRuleCallback>emptyList(),
          new IpAddressPattern("192.168.1.231,222.111.001.002", new String[] { "192.168.1.231", "222.111.001.002" }),
          config.getActionsRepository().getBySignature(ALLOW))));
  }

  @Test
  public void shouldReadCorrectMatcherWildcardfItIsDefinedInConfigFile() throws Exception {
    // given
    final String configFileWithPercentAsWildcard = "besecure_complete_test.properties";
    final String configFileWithTextAsWildcard = "besecure_not_complete_test.properties";
    final ConfigReader configReader = prepareConfigReader();

    // when
    final BeSecureFirewallConfig configWithPercentAsWildcard = configReader.readConfigFromClassPathResource(
      configFileWithPercentAsWildcard);
    final BeSecureFirewallConfig configWithTextAsWildcard = configReader.readConfigFromClassPathResource(
      configFileWithTextAsWildcard);

    // then
    assertThat(configWithPercentAsWildcard, notNullValue());
    assertThat(configWithPercentAsWildcard.isDirty(), equalTo(false));
    assertThat(configWithPercentAsWildcard.hasMatchersWildcardDefined(), equalTo(true));
    assertThat(configWithPercentAsWildcard.getMatchersWildcard(), equalTo("%"));

    assertThat(configWithTextAsWildcard, notNullValue());
    assertThat(configWithTextAsWildcard.isDirty(), equalTo(false));
    assertThat(configWithPercentAsWildcard.hasMatchersWildcardDefined(), equalTo(true));
    assertThat(configWithTextAsWildcard.getMatchersWildcard(), equalTo("WILD_CARD"));

  }

  @Test
  public void shouldReturnNoMatchersWildcardIfNotDefinedInConfigFile() throws Exception {
    // given
    final String configFile = "besecure_empty_test.properties";
    final ConfigReader configReader = prepareConfigReader();

    // when
    final BeSecureFirewallConfig config = configReader.readConfigFromClassPathResource(configFile);

    // then
    assertThat(config, notNullValue());
    assertThat(config.isDirty(), equalTo(false));
    assertThat(config.hasMatchersWildcardDefined(), equalTo(false));
    assertThat(config.getMatchersWildcard(), nullValue());
  }

  @Test
  public void shouldReadCorrectDefaultPolicyIfItIsDefinedInConfigFile() throws Exception {
    // given
    final String configFileWithAllowActionSetAsDefaultPolicy = "besecure_complete_test.properties";
    final String configFileWithDenyActionSetAsDefaultPolicy = "besecure_not_complete_test.properties";
    final ConfigReader configReader = prepareConfigReader();

    // when
    final BeSecureFirewallConfig configWithAllowPolicy = configReader.readConfigFromClassPathResource(
      configFileWithAllowActionSetAsDefaultPolicy);
    final BeSecureFirewallConfig configWithDenyPolicy = configReader.readConfigFromClassPathResource(
      configFileWithDenyActionSetAsDefaultPolicy);

    // then
    final FirewallAction allowFirewallAction = new AllowFirewallAction();
    final FirewallAction denyFirewallAction = new DenyFirewallAction();

    assertThat(configWithAllowPolicy, notNullValue());
    assertThat(configWithAllowPolicy.isDirty(), equalTo(false));
    assertThat(configWithAllowPolicy.getDefaultPolicyAction(),
      equalTo(configWithAllowPolicy.getActionsRepository().getBySignature(ALLOW)));

    assertThat(configWithDenyPolicy, notNullValue());
    assertThat(configWithDenyPolicy.isDirty(), equalTo(false));
    assertThat(configWithDenyPolicy.getDefaultPolicyAction(),
      equalTo(configWithDenyPolicy.getActionsRepository().getBySignature(DENY)));

  }

  @Test
  public void shouldReturnAllowFirewallActionInCasePolicyWasNotDefinedInConfigFile() throws Exception {
    // given
    final String configFileWithoutDefaultPolicyAction = "besecure_empty_test.properties";
    final ConfigReader configReader = prepareConfigReader();

    // when
    final BeSecureFirewallConfig config = configReader.readConfigFromClassPathResource(
      configFileWithoutDefaultPolicyAction);

    // then
    final FirewallAction allowFirewallAction = new AllowFirewallAction();

    assertThat(config, notNullValue());
    assertThat(config.isDirty(), equalTo(false));
    assertThat(config.getDefaultPolicyAction(), equalTo(config.getActionsRepository().getBySignature(ALLOW)));

  }

  @Test
  public void shouldReadAllRulesInOrderTheyAppearInConfigFile() throws Exception {
    // given
    final String configFile = "besecure_complete_test.properties";
    final ConfigReader configReader = prepareConfigReader();

    // when
    final BeSecureFirewallConfig config = configReader.readConfigFromClassPathResource(configFile);

    // then
    final FirewallRuleCallback parsedCallback = config.getCallbacksRepository()
      .getByConfigSignature("COUNTER_CALLBACK");

    assertThat(config, notNullValue());
    assertThat(config.isDirty(), equalTo(false));
    assertThat(parsedCallback, notNullValue());
    assertThat(MatchesCounterCallback.class.isInstance(parsedCallback), equalTo(true));

    assertThat(config.getFirewallRules().get(0),
      equalTo(
        new FirewallRule("drop0Char",
          new UrlMatcher(),
          Collections.<FirewallRuleCallback>emptyList(),
          new StringPattern(new String(new char[] { (char) 0 })),
          config.getActionsRepository().getBySignature(DROP))));
    assertThat(config.getFirewallRules().get(1),
      equalTo(
        new FirewallRule("someRule2",
          new ServletPathMatcher(),
          asList(parsedCallback),
          new StringPattern(new String("eviltext")),
          config.getActionsRepository().getBySignature(DENY))));
    assertThat(config.getFirewallRules().get(2),
      equalTo(
        new FirewallRule("allowRule",
          new ServletPathMatcher(),
          Collections.<FirewallRuleCallback>emptyList(),
          new StringPattern(new String(new char[] { (char) 0 })),
          config.getActionsRepository().getBySignature(ALLOW))));

  }

  public static ConfigReader prepareConfigReader() throws NoSuchFieldException, IllegalAccessException {
    final ConfigReader configReader = new ConfigReader();
    final Field resourceLoadingClass = ConfigReader.class.getDeclaredField("resourceLoadingClass");
    resourceLoadingClass.setAccessible(true);
    resourceLoadingClass.set(configReader, ConfigReaderTest.class);
    return configReader;
  }


}
