package net.secodo.besecurefirewall;

import net.secodo.besecurefirewall.action.ActionResult;
import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.action.impl.AllowFirewallAction;
import net.secodo.besecurefirewall.action.impl.DenyFirewallAction;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.testdata.callback.MatchesCounterCallback;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.callback.FirewallRuleCallback;
import net.secodo.besecurefirewall.rule.matcher.RuleMatcher;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Arrays.asList;
import static java.util.Collections.EMPTY_LIST;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


public class BeSecureFirewallTest {
  private BeSecureFirewall beSecureFirewall;
  private FilterChain filterChain;
  private HttpServletRequest request;
  private HttpServletResponse response;

  private RuleMatcher alwaysMatchingMatcher;
  private RuleMatcher neverMatchingMatcher;

  private FirewallAction allowAction;
  private FirewallAction denyAction;

  private DetectionPattern<?> detectionPattern;


  @Before
  public void setUp() {
    beSecureFirewall = new BeSecureFirewall();
    filterChain = mock(FilterChain.class);
    request = mock(HttpServletRequest.class);
    response = mock(HttpServletResponse.class);

    alwaysMatchingMatcher = mock(RuleMatcher.class);
    neverMatchingMatcher = mock(RuleMatcher.class);

    when(alwaysMatchingMatcher.matches(any(ServletRequestWrapper.class), any(DetectionPattern.class))).thenReturn(true);
    when(neverMatchingMatcher.matches(any(ServletRequestWrapper.class), any(DetectionPattern.class))).thenReturn(false);

    allowAction = new AllowFirewallAction();
    denyAction = new DenyFirewallAction();

    detectionPattern = mock(DetectionPattern.class);

  }

  @Test
  public void shouldAllowRequestInCaseConfigIsNotInitialized() throws Exception {
    // given
    BeSecureFirewallConfig config = BeSecureFirewallConfig.createDirtyConfig();
    beSecureFirewall.setConfig(config);

    // when
    beSecureFirewall.doFilter(request, response, filterChain);

    // then
    verify(filterChain, times(1)).doFilter(request, response);
  }


  @Test
  public void shouldAllowRequestInCaseNoRuleWasDefined() throws Exception {
    // given
    BeSecureFirewallConfig config = new BeSecureFirewallConfig(); // no rules added
    beSecureFirewall.setConfig(config);

    // when
    beSecureFirewall.doFilter(request, response, filterChain);

    // then
    verify(filterChain, times(1)).doFilter(request, response);
  }

  @Test
  public void shouldDenyRequestInCaseNoRuleWasDefinedButDefaultFirewallPolicyChangedToDeny() throws Exception {
    // given
    BeSecureFirewallConfig config = new BeSecureFirewallConfig(); // no rules added
    beSecureFirewall.setConfig(config);

    config.setDefaultPolicyAction(new DenyFirewallAction());

    // when
    beSecureFirewall.doFilter(request, response, filterChain);

    // then
    verify(filterChain, times(0)).doFilter(request, response);
  }

  @Test
  public void shouldDenyRequestInCaseOneRuleDecidedToDeny() throws Exception {
    // given
    BeSecureFirewallConfig config = new BeSecureFirewallConfig(); // no rules added
    beSecureFirewall.setConfig(config);

    FirewallRule rule1 = new FirewallRule("rule1",
      neverMatchingMatcher,
      detectionPattern,
      allowAction);
    FirewallRule rule2 = new FirewallRule("rule2",
      alwaysMatchingMatcher,
      detectionPattern,
      denyAction);
    FirewallRule rule3 = new FirewallRule("rule3",
      alwaysMatchingMatcher,
      detectionPattern,
      allowAction);

    config.addRules(rule1, rule2, rule3);

    // when
    beSecureFirewall.doFilter(request, response, filterChain);

    // then
    verify(filterChain, times(0)).doFilter(request, response);
    verify(neverMatchingMatcher, times(1)).matches(any(ServletRequestWrapper.class), any(DetectionPattern.class)); // call for rule1
    verify(alwaysMatchingMatcher, times(1)).matches(any(ServletRequestWrapper.class), any(DetectionPattern.class)); // call for rule2, but not for rule 3
  }

  @Test
  public void shouldAllowRequestInCaseNoneOfRulesMatched() throws Exception {
    // given
    BeSecureFirewallConfig config = new BeSecureFirewallConfig(); // no rules added
    beSecureFirewall.setConfig(config);

    FirewallRule rule1 = new FirewallRule("rule1",
      neverMatchingMatcher,
      detectionPattern,
      allowAction);
    FirewallRule rule2 = new FirewallRule("rule2", neverMatchingMatcher, detectionPattern, denyAction);
    FirewallRule rule3 = new FirewallRule("rule3",
      neverMatchingMatcher,
      detectionPattern,
      allowAction);

    config.addRules(rule1, rule2, rule3);

    // when
    beSecureFirewall.doFilter(request, response, filterChain);

    // then
    verify(filterChain, times(1)).doFilter(request, response); // allowed
    verify(neverMatchingMatcher, times(3)).matches(any(ServletRequestWrapper.class), any(DetectionPattern.class)); // call for rule1, rule2, rule3
    verify(alwaysMatchingMatcher, times(0)).matches(any(ServletRequestWrapper.class), any(DetectionPattern.class)); //
  }

  @Test
  public void shouldProcessNextFirewallRuleInCaseActionResultSaysThatRequestWasNotProcessed() throws Exception {
    // given
    final BeSecureFirewallConfig config = new BeSecureFirewallConfig();

    final FirewallAction actionWithRequestNOTHandledResult = getMockFirewallActionWithResult(
      ActionResult.REQUEST_NOT_HANDLED);
    final FirewallAction actionWithRequestHandledResult = getMockFirewallActionWithResult(ActionResult.REQUEST_HANDLED);

    final RuleMatcher alwaysMatchingMatcher = mock(RuleMatcher.class);
    when(alwaysMatchingMatcher.matches(any(ServletRequestWrapper.class), any(DetectionPattern.class))).thenReturn(true);

    final FirewallRule rule1 = mock(FirewallRule.class);
    when(rule1.getMatcher()).thenReturn(alwaysMatchingMatcher);
    when(rule1.getOnMatchCallbacks()).thenReturn(Collections.<FirewallRuleCallback>emptyList());
    when(rule1.getAction()).thenReturn(actionWithRequestNOTHandledResult);

    final FirewallRule rule2 = mock(FirewallRule.class);
    when(rule2.getMatcher()).thenReturn(alwaysMatchingMatcher);
    when(rule2.getOnMatchCallbacks()).thenReturn(Collections.<FirewallRuleCallback>emptyList());
    when(rule2.getAction()).thenReturn(actionWithRequestNOTHandledResult);

    final FirewallRule rule3 = mock(FirewallRule.class);
    when(rule3.getMatcher()).thenReturn(alwaysMatchingMatcher);
    when(rule3.getOnMatchCallbacks()).thenReturn(Collections.<FirewallRuleCallback>emptyList());
    when(rule3.getAction()).thenReturn(actionWithRequestHandledResult); // this rule should handle the request

    final FirewallRule rule4 = mock(FirewallRule.class); // this rule should never be evaluated since the request should be handled while evaluating rule3
    when(rule4.getMatcher()).thenReturn(alwaysMatchingMatcher);
    when(rule4.getOnMatchCallbacks()).thenReturn(Collections.<FirewallRuleCallback>emptyList());
    when(rule4.getAction()).thenReturn(actionWithRequestHandledResult);

    config.addRules(rule1, rule2, rule3, rule4);

    beSecureFirewall.setConfig(config);

    // when
    beSecureFirewall.doFilter(mock(ServletRequest.class), mock(ServletResponse.class), mock(FilterChain.class));

    // then
    verify(rule1).getMatcher(); // since this rule should be evaluted than firewall should try to access the matcher
    verify(rule2).getMatcher(); // since this rule should be evaluted than firewall should try to access the matcher
    verify(rule3).getMatcher(); // since this rule should be evaluted than firewall should try to access the matcher
    verifyZeroInteractions(rule4); // since this rule should be evaluted than firewall should try to access the matcher

  }

  private FirewallAction getMockFirewallActionWithResult(ActionResult requestNotHandled) throws IOException,
    ServletException {
    final FirewallAction actionWithRequestNOTHandledResult = mock(FirewallAction.class);
    when(
      actionWithRequestNOTHandledResult.handleRequest(any(BeSecureFirewallConfig.class), any(ServletRequestWrapper
          .class),
        any(ServletResponseWrapper.class),
        any(FilterChain.class))).thenReturn(requestNotHandled);
    return actionWithRequestNOTHandledResult;
  }

  // ======================== onMatchCallback tests=============================== //

  @Test
  public void shouldNOTExecuteANYCallbackInCaseRequestDidNotMatch() throws Exception {
    // given
    final BeSecureFirewallConfig config = new BeSecureFirewallConfig();

    final RuleMatcher matcher = mock(RuleMatcher.class);
    when(matcher.matches(any(ServletRequestWrapper.class), any(DetectionPattern.class))).thenReturn(false);

    final FirewallAction action = getMockFirewallActionWithResult(ActionResult.REQUEST_HANDLED);

    FirewallRule rule = new FirewallRule("rule1", matcher, mock(DetectionPattern.class), action);

    final FirewallRuleCallback mockDefaultCallback = mock(FirewallRuleCallback.class);

    addNewCallback(rule, mockDefaultCallback);

    config.addRule(rule);

    beSecureFirewall.setConfig(config);

    // when
    beSecureFirewall.doFilter(mock(ServletRequest.class), mock(ServletResponse.class), mock(FilterChain.class));

    // then
    verifyZeroInteractions(mockDefaultCallback);
  }

  @Test
  public void shouldExecuteCustomCallbackInCaseRequestMatchedAndCustomCallbackWasDefined() throws Exception {
    // given
    final BeSecureFirewallConfig config = new BeSecureFirewallConfig();

    final RuleMatcher matcher = mock(RuleMatcher.class);
    when(matcher.matches(any(ServletRequestWrapper.class), any(DetectionPattern.class))).thenReturn(true);

    final FirewallAction action = getMockFirewallActionWithResult(ActionResult.REQUEST_HANDLED);


    final AtomicBoolean callbackExecuted = new AtomicBoolean(false);

    final FirewallRuleCallback myCallback = new FirewallRuleCallback() {
      @Override
      public void call(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper, FirewallRule rule) {
        callbackExecuted.set(true);
      }
    };

    FirewallRule rule = new FirewallRule("rule1", matcher, asList(myCallback), mock(DetectionPattern.class), action);

    config.addRule(rule);

    beSecureFirewall.setConfig(config);

    // when
    beSecureFirewall.doFilter(mock(ServletRequest.class), mock(ServletResponse.class), mock(FilterChain.class));

    // then
    assertThat(callbackExecuted.get(), equalTo(true));
  }

  @Test
  public void shouldNOTExecuteCustomCallbackInCaseRequestNOTMatchedEvenThoughCustomCallbackWasDefined()
    throws Exception {
    // given
    final BeSecureFirewallConfig config = new BeSecureFirewallConfig();

    final RuleMatcher matcher = mock(RuleMatcher.class);
    when(matcher.matches(any(ServletRequestWrapper.class), any(DetectionPattern.class))).thenReturn(false);

    final FirewallAction action = getMockFirewallActionWithResult(ActionResult.REQUEST_HANDLED);


    final AtomicBoolean callbackExecuted = new AtomicBoolean(false);

    final FirewallRuleCallback myCallback = new FirewallRuleCallback() {
      @Override
      public void call(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper, FirewallRule rule) {
        callbackExecuted.set(true);
      }
    };

    FirewallRule rule = new FirewallRule("rule1", matcher, asList(myCallback), mock(DetectionPattern.class), action);
    config.addRule(rule);

    beSecureFirewall.setConfig(config);

    // when
    beSecureFirewall.doFilter(mock(ServletRequest.class), mock(ServletResponse.class), mock(FilterChain.class));

    // then
    assertThat(callbackExecuted.get(), equalTo(false));


  }


  private void addNewCallback(FirewallRule rule, FirewallRuleCallback callbackToAdd)
    throws NoSuchFieldException, IllegalAccessException {
    final Field onMatchCallbacksField = FirewallRule.class.getDeclaredField("onMatchCallbacks");
    onMatchCallbacksField.setAccessible(true);
    List<FirewallRuleCallback> firewallRuleCallbacks = getFirewallRuleCallbacks(rule, onMatchCallbacksField);
    List<FirewallRuleCallback> callbacks = new ArrayList<FirewallRuleCallback>(firewallRuleCallbacks);
    callbacks.add(callbackToAdd);
  }

  @SuppressWarnings("unchecked")
  private List<FirewallRuleCallback> getFirewallRuleCallbacks(FirewallRule rule, Field onMatchCallbacksField) throws IllegalAccessException {
    return (List<FirewallRuleCallback>) onMatchCallbacksField.get(rule);
  }


  // ======================== init method test ================================== //

  @Test
  public void shouldNotReadConfigInCaseConfigWasAlreadySetUpManually() throws Exception {
    // given
    BeSecureFirewallConfig config = new BeSecureFirewallConfig(); // no rules added

    FirewallRule rule = Mockito.mock(FirewallRule.class);

    config.addRule(rule);
    beSecureFirewall.setConfig(config);

    FilterConfig filterConfig = mock(FilterConfig.class);

    // when
    beSecureFirewall.init(filterConfig);

    // then
    verify(filterConfig, times(0)).getInitParameter(any(String.class)); // name of config file is read from init parameter
    assertTrue(readConfig(beSecureFirewall) == config); // config not overwritten
  }

  @Test
  public void shouldReadConfigInCaseConfigWasNoSetUpManually() throws Exception {
    // given
    FilterConfig filterConfig = mock(FilterConfig.class);
    when(filterConfig.getInitParameter("configfile")).thenReturn("besecure_complete_test.properties");

    // when
    beSecureFirewall.init(filterConfig);

    // then
    verify(filterConfig, times(1)).getInitParameter(any(String.class)); // name of config file is read from init parameter

    final BeSecureFirewallConfig config = readConfig(beSecureFirewall);

    assertThat(findRuleByName(config.getFirewallRules(), "drop0Char"), notNullValue());
    assertThat(findRuleByName(config.getFirewallRules(), "someRule2"), notNullValue());
    assertThat(findRuleByName(config.getFirewallRules(), "allowRule"), notNullValue());

    assertThat(findRuleByName(config.getFirewallRules(), "someRule2").getOnMatchCallbacks().size(), equalTo(1));
    assertThat(findRuleByName(config.getFirewallRules(), "someRule2").getOnMatchCallbacks().get(0), instanceOf(MatchesCounterCallback.class));
  }

  @Test
  public void shouldCreateFirewallWithDirtyConfigInCaseConfigWasNotSetUpManuallyAndNoConfigFilenameWasSetAsInitParameter()
    throws Exception {
    // given
    FilterConfig filterConfig = mock(FilterConfig.class);
    when(filterConfig.getInitParameter("configfile")).thenReturn(null);

    // when
    beSecureFirewall.init(filterConfig);

    // then
    verify(filterConfig, times(1)).getInitParameter(any(String.class)); // name of config file is read from init parameter

    final BeSecureFirewallConfig config = readConfig(beSecureFirewall);

    assertThat(config.isDirty(), equalTo(true));

  }

  @Test
  public void shouldNotFailWhenCallingDestroyMethod() throws Exception {
    beSecureFirewall.destroy();
  }

  private FirewallRule findRuleByName(List<FirewallRule> firewallRules, String ruleName) {
    for (FirewallRule rule : firewallRules) {
      if (rule.getName().equals(ruleName)) {
        return rule;
      }
    }

    return null;
  }


  private BeSecureFirewallConfig readConfig(BeSecureFirewall beSecureFirewall) throws NoSuchFieldException,
    IllegalAccessException {
    final Field configField = BeSecureFirewall.class.getDeclaredField("config");
    configField.setAccessible(true);

    return (BeSecureFirewallConfig) configField.get(beSecureFirewall);

  }
}
