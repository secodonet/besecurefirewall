package net.secodo.besecurefirewall.testdata.callback;

import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.callback.FirewallRuleCallback;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;


public class MatchesCounterCallback implements FirewallRuleCallback {
  private int numberOfMatches;

  public MatchesCounterCallback() {
  }


  @Override
  public void call(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper, FirewallRule rule) {
    numberOfMatches++;
  }

  public int getNumberOfMatches() {
    return numberOfMatches;
  }

}
