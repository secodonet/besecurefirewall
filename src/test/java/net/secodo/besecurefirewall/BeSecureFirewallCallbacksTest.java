package net.secodo.besecurefirewall;

import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.config.ConfigReader;
import net.secodo.besecurefirewall.testdata.callback.MatchesCounterCallback;
import net.secodo.besecurefirewall.rule.callback.FirewallCallbacksRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static net.secodo.besecurefirewall.config.ConfigReaderTest.prepareConfigReader;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BeSecureFirewallCallbacksTest {

  @Mock
  private HttpServletRequest servletRequest;

  @Mock
  private HttpServletResponse servletResponse;

  @Mock
  private FilterChain filterChain;


  @Test
  public void shouldCallCounterCallbackForRule2AsDefinedInPropertiesConfigFile() throws Exception {
    // given
    final String configFile = "besecure_complete_test.properties";
    final ConfigReader configReader = prepareConfigReader();
    final BeSecureFirewallConfig config = configReader.readConfigFromClassPathResource(configFile);

    final BeSecureFirewall beSecureFirewall = new BeSecureFirewall();
    beSecureFirewall.setConfig(config);

    final FirewallCallbacksRepository callbacksRepository = config.getCallbacksRepository();
    final MatchesCounterCallback counterCallback = (MatchesCounterCallback) callbacksRepository.getByConfigSignature("COUNTER_CALLBACK");
    assert counterCallback.getNumberOfMatches() == 0;

    final String servletContext = "/some";
    final String requestUri = servletContext + "/servlet/eviltext/aa";
    when(servletRequest.getRequestURI()).thenReturn(requestUri); // evil text matches rule2
    when(servletRequest.getContextPath()).thenReturn(servletContext);
    when(servletRequest.getRequestURL()).thenReturn(new StringBuffer("http://my.server.com" + requestUri));

    // when
    beSecureFirewall.doFilter(servletRequest, servletResponse, filterChain);

    // then
    assertThat(counterCallback.getNumberOfMatches(), equalTo(1));

  }

  @Test
  public void shouldCallCounterCallback2TimesAsDefinedForRule2AndAllowRuleInPropertiesConfigFile() throws Exception {
    // given
    final String configFile = "besecure_complete_test.properties";
    final ConfigReader configReader = prepareConfigReader();
    final BeSecureFirewallConfig config = configReader.readConfigFromClassPathResource(configFile);

    final BeSecureFirewall beSecureFirewall = new BeSecureFirewall();
    beSecureFirewall.setConfig(config);

    final FirewallCallbacksRepository callbacksRepository = config.getCallbacksRepository();
    final MatchesCounterCallback counterCallback = (MatchesCounterCallback) callbacksRepository.getByConfigSignature("COUNTER_CALLBACK");
    assert counterCallback.getNumberOfMatches() == 0;

    final String servletContext1 = "/some";
    final String requestUri1 = servletContext1 + "/servlet/eviltext/aa";
    final String servletContext2 = "/boo";
    final String requestUri2 = servletContext2 + "/servlet/eviltext/any:300aa";

    // when
    when(servletRequest.getRequestURI()).thenReturn(requestUri1); // evil text matches rule2
    when(servletRequest.getContextPath()).thenReturn(servletContext1);
    when(servletRequest.getRequestURL()).thenReturn(new StringBuffer("http://my.server.com" + requestUri1));

    beSecureFirewall.doFilter(servletRequest, servletResponse, filterChain); // match someRule2

    when(servletRequest.getRequestURI()).thenReturn(requestUri2);
    when(servletRequest.getContextPath()).thenReturn(servletContext2);
    when(servletRequest.getRequestURL()).thenReturn(new StringBuffer("http://my.server.com" + requestUri2));

    beSecureFirewall.doFilter(servletRequest, servletResponse, filterChain); // match colonRule

    // then
    assertThat(counterCallback.getNumberOfMatches(), equalTo(2));

  }

  @Test
  public void should() throws Exception {
    // given


    // when

    // then

  }

}
