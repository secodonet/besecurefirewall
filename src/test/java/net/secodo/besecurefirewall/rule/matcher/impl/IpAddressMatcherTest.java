package net.secodo.besecurefirewall.rule.matcher.impl;

import net.secodo.besecurefirewall.rule.matcher.pattern.impl.IpAddressPattern;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import net.secodo.besecurefirewall.rule.matcher.pattern.impl.StringPattern;
import javax.servlet.http.HttpServletRequest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;


public class IpAddressMatcherTest {
  private ServletRequestWrapper requestWrapper;

  @Before
  public void setUp() {
    requestWrapper = new ServletRequestWrapper(Mockito.mock(HttpServletRequest.class));
  }


  @Test
  public void shouldCorrectlyMatchRequestIfRequestComesFromGivenIpAddress() {
    // given
    final IpAddressMatcher matcher = new IpAddressMatcher();

    final HttpServletRequest requestMock = (HttpServletRequest) requestWrapper.getServletRequest();

    when(requestMock.getRemoteAddr()).thenReturn("192.168.1.220");

    // then
    assertThat(matcher.matches(requestWrapper, new StringPattern("192.168.1.220")), equalTo(true));
    assertThat(matcher.matches(requestWrapper, new StringPattern("192.168.1.221")), equalTo(false));

    assertThat(
      matcher.matches(requestWrapper,
        new IpAddressPattern("192.168.1.220, 192.168.1.255", new String[] { "192.168.1.220", "192.168.1.255" })),
      equalTo(true));
    assertThat(
      matcher.matches(requestWrapper,
        new IpAddressPattern("192.168.1.221, 192.168.1.255", new String[] { "192.168.1.221", "192.168.1.255" })),
      equalTo(false));


  }

}
