package net.secodo.besecurefirewall.rule.matcher.impl;

import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import net.secodo.besecurefirewall.rule.matcher.pattern.impl.StringPattern;
import javax.servlet.http.HttpServletRequest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;


public class ServletPathMatcherTest {
  private ServletRequestWrapper requestWrapper;

  @Before
  public void setUp() {
    requestWrapper = new ServletRequestWrapper(Mockito.mock(HttpServletRequest.class));
  }


  @Test
  public void shouldCorrectlyMatchRequestIfSpecifiedPatternIsPresentWithinServletPath() {
    // given
    final ServletPathMatcher matcher = new ServletPathMatcher();

    final HttpServletRequest mockRequest = (HttpServletRequest) requestWrapper.getServletRequest();

    when(mockRequest.getRequestURL()).thenReturn(new StringBuffer("http://my.server:7080/myapp/my/servlet/path")); // should not matter
    when(mockRequest.getRequestURI()).thenReturn("/myapp/my/servlet/path");
    when(mockRequest.getContextPath()).thenReturn("/myapp");

    // when
    final boolean matches = matcher.matches(requestWrapper, new StringPattern("/my/"));
    final boolean matches2 = matcher.matches(requestWrapper, new StringPattern("/"));
    final boolean matches3 = matcher.matches(requestWrapper, new StringPattern("b"));
    final boolean matches4 = matcher.matches(requestWrapper, new StringPattern("p/"));
    final boolean matches5 = matcher.matches(requestWrapper, new StringPattern("myap"));
    final boolean matches6 = matcher.matches(requestWrapper, new StringPattern("server"));
    final boolean matches7 = matcher.matches(requestWrapper, new StringPattern(":"));
    final boolean matches8 = matcher.matches(requestWrapper, new StringPattern("0"));
    final boolean matches9 = matcher.matches(requestWrapper, new StringPattern("7080"));
    final boolean matches10 = matcher.matches(requestWrapper, new StringPattern("//"));
    final boolean matches11 = matcher.matches(requestWrapper, new StringPattern("/my/servlet/path"));


    assertThat(matches, equalTo(true));
    assertThat(matches2, equalTo(true));
    assertThat(matches3, equalTo(false));
    assertThat(matches4, equalTo(false));
    assertThat(matches5, equalTo(false));
    assertThat(matches6, equalTo(false));
    assertThat(matches7, equalTo(false));
    assertThat(matches8, equalTo(false));
    assertThat(matches9, equalTo(false));
    assertThat(matches9, equalTo(false));
    assertThat(matches10, equalTo(false));
    assertThat(matches11, equalTo(true));


  }

  @Test
  public void shouldCorrectlyMatchRequestIfUriEncodedCharactersAreUsedWithinServletPath() {
    // given
    final ServletPathMatcher matcher = new ServletPathMatcher();
    final HttpServletRequest mockRequest = (HttpServletRequest) requestWrapper.getServletRequest();

    when(mockRequest.getRequestURL()).thenReturn(
      new StringBuffer("http://www.example.com/d%C3%BCssel%64orf?neighbourhood=L%C3%B6rick")); // should not matter

    when(mockRequest.getRequestURI()).thenReturn("/d%C3%BCsseldorf");
    when(mockRequest.getContextPath()).thenReturn("");

    // when
    final boolean matches = matcher.matches(requestWrapper, new StringPattern("düsseldorf"));
    final boolean matches2 = matcher.matches(requestWrapper, new StringPattern("/"));
    final boolean matches3 = matcher.matches(requestWrapper, new StringPattern("b"));
    final boolean matches4 = matcher.matches(requestWrapper, new StringPattern("p/"));
    final boolean matches5 = matcher.matches(requestWrapper, new StringPattern("myap"));
    final boolean matches6 = matcher.matches(requestWrapper, new StringPattern("server"));
    final boolean matches7 = matcher.matches(requestWrapper, new StringPattern(":"));
    final boolean matches8 = matcher.matches(requestWrapper, new StringPattern("0"));
    final boolean matches9 = matcher.matches(requestWrapper, new StringPattern("7080"));
    final boolean matches10 = matcher.matches(requestWrapper, new StringPattern("//"));
    final boolean matches11 = matcher.matches(requestWrapper, new StringPattern("/my/servlet/path"));
    final boolean matches12 = matcher.matches(requestWrapper, new StringPattern("Lörick"));


    assertThat(matches, equalTo(true));
    assertThat(matches2, equalTo(true));
    assertThat(matches3, equalTo(false));
    assertThat(matches4, equalTo(false));
    assertThat(matches5, equalTo(false));
    assertThat(matches6, equalTo(false));
    assertThat(matches7, equalTo(false));
    assertThat(matches8, equalTo(false));
    assertThat(matches9, equalTo(false));
    assertThat(matches9, equalTo(false));
    assertThat(matches10, equalTo(false));
    assertThat(matches11, equalTo(false));
    assertThat(matches12, equalTo(false)); // this is only servlet path matcher - it does not match query params


  }

  @Test
  public void shouldCorrectlyMatchRequestIfUriEncodedCharactersAreUsedToEncodeWholeServletPath() {
    // given
    final ServletPathMatcher matcher = new ServletPathMatcher();
    final HttpServletRequest mockRequest = (HttpServletRequest) requestWrapper.getServletRequest();

    when(mockRequest.getRequestURL()).thenReturn(
      new StringBuffer(
        "%68%74%74%70%3A%2F%2F%77%77%77%2E%65%78%61%6D%70%6C%65%2E%63%6F%6D%2F%64%C3%BC%73%73%65%6C%64%6F%72%66%3F%6E%65%69%67%68%62%6F%75%72%68%6F%6F%64%3D%4C%C3%B6%72%69%63%6B"));
    // http://www.example.com/düsseldorf?neighbourhood=Lörick

    when(mockRequest.getRequestURI()).thenReturn("/d%C3%BCsseldorf");
    when(mockRequest.getContextPath()).thenReturn("");

    // when
    final boolean matches = matcher.matches(requestWrapper, new StringPattern("düsseldorf"));
    final boolean matches2 = matcher.matches(requestWrapper, new StringPattern("/"));
    final boolean matches3 = matcher.matches(requestWrapper, new StringPattern("b"));
    final boolean matches4 = matcher.matches(requestWrapper, new StringPattern("p/"));
    final boolean matches5 = matcher.matches(requestWrapper, new StringPattern("myap"));
    final boolean matches6 = matcher.matches(requestWrapper, new StringPattern("server"));
    final boolean matches7 = matcher.matches(requestWrapper, new StringPattern(":"));
    final boolean matches8 = matcher.matches(requestWrapper, new StringPattern("0"));
    final boolean matches9 = matcher.matches(requestWrapper, new StringPattern("7080"));
    final boolean matches10 = matcher.matches(requestWrapper, new StringPattern("//"));
    final boolean matches11 = matcher.matches(requestWrapper, new StringPattern("/my/servlet/path"));
    final boolean matches12 = matcher.matches(requestWrapper, new StringPattern("Lörick"));


    assertThat(matches, equalTo(true));
    assertThat(matches2, equalTo(true));
    assertThat(matches3, equalTo(false));
    assertThat(matches4, equalTo(false));
    assertThat(matches5, equalTo(false));
    assertThat(matches6, equalTo(false));
    assertThat(matches7, equalTo(false));
    assertThat(matches8, equalTo(false));
    assertThat(matches9, equalTo(false));
    assertThat(matches9, equalTo(false));
    assertThat(matches10, equalTo(false));
    assertThat(matches11, equalTo(false));
    assertThat(matches12, equalTo(false)); // this is only servlet path matcher - it does not match query params


  }
}
