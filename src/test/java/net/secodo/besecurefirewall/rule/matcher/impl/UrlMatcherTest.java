package net.secodo.besecurefirewall.rule.matcher.impl;

import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import net.secodo.besecurefirewall.rule.matcher.pattern.impl.StringPattern;
import javax.servlet.http.HttpServletRequest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;


public class UrlMatcherTest {
  private ServletRequestWrapper requestWrapper;

  @Before
  public void setUp() {
    requestWrapper = new ServletRequestWrapper(Mockito.mock(HttpServletRequest.class));
  }


  @Test
  public void shouldCorrectlyMatchRequestIfSpecifiedPatternIsPresentWithinServletPath() {
    // given
    final UrlMatcher matcher = new UrlMatcher();

    final HttpServletRequest requestMock = (HttpServletRequest) requestWrapper.getServletRequest();

    when(requestMock.getRequestURL()).thenReturn(new StringBuffer("http://my.server:7080/myapp/my/servlet/path"));
    when(requestMock.getContextPath()).thenReturn("/myapp"); // should not matter
    when(requestMock.getQueryString()).thenReturn(null);

    // when
    final boolean matches = matcher.matches(requestWrapper, new StringPattern("/my/"));
    final boolean matches2 = matcher.matches(requestWrapper, new StringPattern("/"));
    final boolean matches3 = matcher.matches(requestWrapper, new StringPattern("b"));
    final boolean matches4 = matcher.matches(requestWrapper, new StringPattern("p/"));
    final boolean matches5 = matcher.matches(requestWrapper, new StringPattern("myap"));
    final boolean matches6 = matcher.matches(requestWrapper, new StringPattern("server"));
    final boolean matches7 = matcher.matches(requestWrapper, new StringPattern(":"));
    final boolean matches8 = matcher.matches(requestWrapper, new StringPattern("0"));
    final boolean matches9 = matcher.matches(requestWrapper, new StringPattern("7080"));
    final boolean matches10 = matcher.matches(requestWrapper, new StringPattern("//"));
    final boolean matches11 = matcher.matches(requestWrapper, new StringPattern("/my/servlet/path"));
    final boolean matches12 = matcher.matches(requestWrapper, new StringPattern("am"));


    assertThat(matches, equalTo(true));
    assertThat(matches2, equalTo(true));
    assertThat(matches3, equalTo(false));
    assertThat(matches4, equalTo(true));
    assertThat(matches5, equalTo(true));
    assertThat(matches6, equalTo(true));
    assertThat(matches7, equalTo(true));
    assertThat(matches8, equalTo(true));
    assertThat(matches9, equalTo(true));
    assertThat(matches9, equalTo(true));
    assertThat(matches10, equalTo(true));
    assertThat(matches11, equalTo(true));
    assertThat(matches12, equalTo(false));


  }

  @Test
  public void shouldCorrectlyMatchRequestIfUriEncodedCharactersAreUsedWithinServletPath() {
    // given
    final UrlMatcher matcher = new UrlMatcher();
    final HttpServletRequest requestMock = (HttpServletRequest) requestWrapper.getServletRequest();


    when(requestMock.getRequestURL()).thenReturn(
      new StringBuffer("http://www.example.com/d%C3%BCssel%64orf"));
    when(requestMock.getContextPath()).thenReturn("");
    when(requestMock.getQueryString()).thenReturn("neighbourhood=L%C3%B6rick");


    // when
    final boolean matches = matcher.matches(requestWrapper, new StringPattern("düsseldorf"));
    final boolean matches2 = matcher.matches(requestWrapper, new StringPattern("/"));
    final boolean matches3 = matcher.matches(requestWrapper, new StringPattern("b"));
    final boolean matches4 = matcher.matches(requestWrapper, new StringPattern("p/"));
    final boolean matches5 = matcher.matches(requestWrapper, new StringPattern("myap"));
    final boolean matches6 = matcher.matches(requestWrapper, new StringPattern("server"));
    final boolean matches7 = matcher.matches(requestWrapper, new StringPattern(":"));
    final boolean matches8 = matcher.matches(requestWrapper, new StringPattern("0"));
    final boolean matches9 = matcher.matches(requestWrapper, new StringPattern("7080"));
    final boolean matches10 = matcher.matches(requestWrapper, new StringPattern("//"));
    final boolean matches11 = matcher.matches(requestWrapper, new StringPattern("/my/servlet/path"));
    final boolean matches12 = matcher.matches(requestWrapper, new StringPattern("Lörick"));
    final boolean matches13 = matcher.matches(requestWrapper, new StringPattern("example"));
    final boolean matches14 = matcher.matches(requestWrapper, new StringPattern("http"));
    final boolean matches15 = matcher.matches(requestWrapper, new StringPattern("https"));


    assertThat(matches, equalTo(true));
    assertThat(matches2, equalTo(true));
    assertThat(matches3, equalTo(true));
    assertThat(matches4, equalTo(false));
    assertThat(matches5, equalTo(false));
    assertThat(matches6, equalTo(false));
    assertThat(matches7, equalTo(true));
    assertThat(matches8, equalTo(false));
    assertThat(matches9, equalTo(false));
    assertThat(matches10, equalTo(true));
    assertThat(matches11, equalTo(false));
    assertThat(matches12, equalTo(true));
    assertThat(matches13, equalTo(true));
    assertThat(matches14, equalTo(true));
    assertThat(matches15, equalTo(false));


  }

  @Test
  public void shouldCorrectlyMatchRequestIfUriEncodedCharactersAreUsedToEncodeWholeServletPath() {
    // given
    final UrlMatcher matcher = new UrlMatcher();
    final HttpServletRequest requestMock = (HttpServletRequest) requestWrapper.getServletRequest();


    when(requestMock.getRequestURL()).thenReturn(
      new StringBuffer(
        "%68%74%74%70%3A%2F%2F%77%77%77%2E%65%78%61%6D%70%6C%65%2E%63%6F%6D%2F%64%C3%BC%73%73%65%6C%64%6F%72%66%3F")); // ends with question mark (%3f)


    when(requestMock.getRequestURI()).thenReturn("/does_not_matter_in_this_case");
    when(requestMock.getContextPath()).thenReturn("");
    when(requestMock.getQueryString()).thenReturn("%6E%65%69%67%68%62%6F%75%72%68%6F%6F%64%3D%4C%C3%B6%72%69%63%6B");

    // http://www.example.com/düsseldorf?neighbourhood=Lörick


    // when
    final boolean matches = matcher.matches(requestWrapper, new StringPattern("düsseldorf"));
    final boolean matches2 = matcher.matches(requestWrapper, new StringPattern("/"));
    final boolean matches3 = matcher.matches(requestWrapper, new StringPattern("b"));
    final boolean matches4 = matcher.matches(requestWrapper, new StringPattern("p/"));
    final boolean matches5 = matcher.matches(requestWrapper, new StringPattern("myap"));
    final boolean matches6 = matcher.matches(requestWrapper, new StringPattern("server"));
    final boolean matches7 = matcher.matches(requestWrapper, new StringPattern(":"));
    final boolean matches8 = matcher.matches(requestWrapper, new StringPattern("0"));
    final boolean matches9 = matcher.matches(requestWrapper, new StringPattern("7080"));
    final boolean matches10 = matcher.matches(requestWrapper, new StringPattern("//"));
    final boolean matches11 = matcher.matches(requestWrapper, new StringPattern("/my/servlet/path"));
    final boolean matches12 = matcher.matches(requestWrapper, new StringPattern("Lörick"));
    final boolean matches13 = matcher.matches(requestWrapper, new StringPattern("example"));
    final boolean matches14 = matcher.matches(requestWrapper, new StringPattern("http"));
    final boolean matches15 = matcher.matches(requestWrapper, new StringPattern("https"));
    final boolean matches16 = matcher.matches(requestWrapper, new StringPattern("??"));
    final boolean matches17 = matcher.matches(requestWrapper, new StringPattern("?"));


    assertThat(matches, equalTo(true));
    assertThat(matches2, equalTo(true));
    assertThat(matches3, equalTo(true));
    assertThat(matches4, equalTo(false));
    assertThat(matches5, equalTo(false));
    assertThat(matches6, equalTo(false));
    assertThat(matches7, equalTo(true));
    assertThat(matches8, equalTo(false));
    assertThat(matches9, equalTo(false));
    assertThat(matches10, equalTo(true));
    assertThat(matches11, equalTo(false));
    assertThat(matches12, equalTo(true));
    assertThat(matches13, equalTo(true));
    assertThat(matches14, equalTo(true));
    assertThat(matches15, equalTo(false));
    assertThat(matches16, equalTo(false));
    assertThat(matches17, equalTo(true));


  }

}
