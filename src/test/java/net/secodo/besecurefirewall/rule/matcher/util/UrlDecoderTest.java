package net.secodo.besecurefirewall.rule.matcher.util;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static net.secodo.besecurefirewall.rule.matcher.util.UrlDecoder.decodePercentEncodedUrl;


public class UrlDecoderTest {
  @Test
  public void testDecodePercentEncodedUrl() throws Exception {
    assertThat(decodePercentEncodedUrl("d%C3%BCsseldorf"), equalTo("düsseldorf"));
    assertThat(decodePercentEncodedUrl("d%C3%BCssel%64orf"), equalTo("düsseldorf"));
    assertThat(decodePercentEncodedUrl("d%C3%BCssel%64%25orf"), equalTo("düsseld%orf"));
    assertThat(decodePercentEncodedUrl("d%25%C3%BCssel%64%25orf"), equalTo("d%üsseld%orf"));
    assertThat(decodePercentEncodedUrl("%C3%BCsseldorf"), equalTo("üsseldorf"));
    assertThat(decodePercentEncodedUrl("%00sseldorf"),
      equalTo(new StringBuffer().append((char) 0).append("sseldorf").toString()));

    assertThat(
      decodePercentEncodedUrl(
        "%68%74%74%70%3A%2F%2F%77%77%77%2E%65%78%61%6D%70%6C%65%2E%63%6F%6D%2F%64%C3%BC%73%73%65%6C%64%6F%72%66%3F%6E%65%69%67%68%62%6F%75%72%68%6F%6F%64%3D%4C%C3%B6%72%69%63%6B"),
      equalTo("http://www.example.com/düsseldorf?neighbourhood=Lörick"));

  }

  @Test
  public void testDecodeInvalidPercentEncodedUrl() throws Exception {
    assertThat(decodePercentEncodedUrl("d%25%C3%25%BCssel%64%25orf"), equalTo("d%%C3%%BCsseld%orf"));
    assertThat(decodePercentEncodedUrl("d%AA%C3%BCsseldorf"), equalTo("d%AAüsseldorf"));
    assertThat(decodePercentEncodedUrl("düsseldorf%AA%C3%BC"), equalTo("düsseldorf%AAü"));
    assertThat(decodePercentEncodedUrl("düsseldorf%A"), equalTo("düsseldorf%A"));

  }

  @Test
  public void testDecodeCharactersOfLength3() throws Exception {
    assertThat(decodePercentEncodedUrl("L%C3%B6rick%ef%be%89"), equalTo("Lörickﾉ"));

  }
}
