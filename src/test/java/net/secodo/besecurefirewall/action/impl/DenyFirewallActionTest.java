package net.secodo.besecurefirewall.action.impl;

import net.secodo.besecurefirewall.action.ActionResult;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import org.junit.Test;

import javax.servlet.FilterChain;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by walker on 23.04.2017.
 */
public class DenyFirewallActionTest {

  @Test
  public void shouldReturnActionHandledForAllowFirewallAction() throws Exception {
    // given
    final DenyFirewallAction denyFirewallAction = new DenyFirewallAction();

    // when
    final ActionResult actionResult = denyFirewallAction.handleRequest(mock(BeSecureFirewallConfig.class),
      mock(ServletRequestWrapper.class), mock(ServletResponseWrapper.class), mock(FilterChain.class));

    // then
    assertThat(actionResult.isRequestHandled(), is(true));

  }

}