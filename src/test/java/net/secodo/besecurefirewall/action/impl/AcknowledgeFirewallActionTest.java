package net.secodo.besecurefirewall.action.impl;

import net.secodo.besecurefirewall.action.ActionResult;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import org.junit.Test;
import javax.servlet.FilterChain;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;


public class AcknowledgeFirewallActionTest {
  @Test
  public void shouldNotHandleRequestForMatchFirewallActionBecauseMatchActionDoesNotHandleRequests() throws Exception {
    // given
    final AcknowledgeFirewallAction acknowledgeFirewallAction = new AcknowledgeFirewallAction();
    final ServletRequestWrapper requestWrapper = mock(ServletRequestWrapper.class);
    final ServletResponseWrapper responseWrapper = mock(ServletResponseWrapper.class);
    final BeSecureFirewallConfig config = mock(BeSecureFirewallConfig.class);
    final FilterChain filterChain = mock(FilterChain.class);

    // when
    final ActionResult actionResult = acknowledgeFirewallAction.handleRequest(config, requestWrapper,
      responseWrapper, filterChain);

    // then
    assertThat(actionResult.isRequestHandled(), equalTo(false));
    verifyZeroInteractions(requestWrapper);
    verifyZeroInteractions(responseWrapper);

  }

  @Test
  public void shouldReturnNoActionHandledForAllowFirewallAction() throws Exception {
    // given
    final AcknowledgeFirewallAction acknowledgeFirewallAction = new AcknowledgeFirewallAction();

    // when
    final ActionResult actionResult = acknowledgeFirewallAction.handleRequest(mock(BeSecureFirewallConfig.class),
      mock(ServletRequestWrapper.class), mock(ServletResponseWrapper.class), mock(FilterChain.class));

    // then
    assertThat(actionResult.isRequestHandled(), is(false));

  }

}
