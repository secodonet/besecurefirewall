package net.secodo.besecurefirewall.action.impl;

import net.secodo.besecurefirewall.action.ActionResult;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import org.hamcrest.core.Is;
import org.junit.Test;

import javax.servlet.FilterChain;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class AllowFirewallActionTest {

  @Test
  public void shouldReturnActionHandledForAllowFirewallAction() throws Exception {
    // given
    final AllowFirewallAction allowFirewallAction = new AllowFirewallAction();

    // when
    final ActionResult actionResult = allowFirewallAction.handleRequest(mock(BeSecureFirewallConfig.class),
      mock(ServletRequestWrapper.class), mock(ServletResponseWrapper.class), mock(FilterChain.class));

    // then
    assertThat(actionResult.isRequestHandled(), is(true));

  }

}