package net.secodo.besecurefirewall.jettybased.tests;

import net.secodo.besecurefirewall.BeSecureFirewall;
import net.secodo.besecurefirewall.action.impl.DenyFirewallAction;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.jettybased.AbstractJettyBasedIT;
import net.secodo.besecurefirewall.jettybased.tests.servlets.OkOnGetServlet;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.matcher.impl.UrlMatcher;
import net.secodo.besecurefirewall.rule.matcher.pattern.impl.StringPattern;
import org.eclipse.jetty.client.api.ContentResponse;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;


public class FirewallWithConfigBuiltAtRuntimeIT extends AbstractJettyBasedIT {
  @Test
  public void shouldRespondWithHttpOkWhenConfigContainsNoRules() throws Exception {
    // given
    registerServlet(OkOnGetServlet.class, "/test");

    ContentResponse response = client.GET(String.format("http://localhost:%d/test", getJettyPort()));
    assertThat(response.getStatus(), equalTo(200));

    // build firewall
    BeSecureFirewallConfig config = new BeSecureFirewallConfig();
    final BeSecureFirewall firewall = new BeSecureFirewall();
    firewall.setConfig(config);

    registerFilter(firewall, "/*");

    // when
    response = client.GET(String.format("http://localhost:%d/test", getJettyPort()));

    // then
    assertThat(response.getStatus(), equalTo(200));

  }

  @Test
  public void shouldRespondWithHttpForbiddenWhenConfigContainsMatchingRuleDenyingAccessToGivenUrl() throws Exception {
    // given
    registerServlet(OkOnGetServlet.class, "/test");

    ContentResponse response = client.GET(String.format("http://localhost:%d/test", getJettyPort()));
    assertThat(response.getStatus(), equalTo(200));

    // build firewall
    BeSecureFirewallConfig config = new BeSecureFirewallConfig();
    config.addRule(
      new FirewallRule("testRule",
        new UrlMatcher(),
        new StringPattern("est"),
        new DenyFirewallAction()));

    final BeSecureFirewall firewall = new BeSecureFirewall();
    firewall.setConfig(config);

    registerFilter(firewall, "/*");

    // when
    response = client.GET(String.format("http://localhost:%d/test", getJettyPort()));

    // then
    assertThat(response.getStatus(), equalTo(403));

  }

  @Test
  public void shouldNOTRespondWithHttpForbiddenWhenConfigContainsNONMatchingRuleDenyingAccess() throws Exception {
    // given
    registerServlet(OkOnGetServlet.class, "/test");

    ContentResponse response = client.GET(String.format("http://localhost:%d/test", getJettyPort()));
    assertThat(response.getStatus(), equalTo(200));

    // build firewall
    BeSecureFirewallConfig config = new BeSecureFirewallConfig();
    config.addRule(
      new FirewallRule("testRule",
        new UrlMatcher(),
        new StringPattern("TTTSDDSD"),
        new DenyFirewallAction()));

    final BeSecureFirewall firewall = new BeSecureFirewall();
    firewall.setConfig(config);

    registerFilter(firewall, "/*");

    // when
    response = client.GET(String.format("http://localhost:%d/test", getJettyPort()));

    // then
    assertThat(response.getStatus(), equalTo(200));

  }
}
