package net.secodo.besecurefirewall.jettybased.tests;

import net.secodo.besecurefirewall.jettybased.AbstractJettyBasedIT;
import net.secodo.besecurefirewall.jettybased.tests.servlets.OkOnGetServlet;
import org.eclipse.jetty.client.api.ContentResponse;
import org.junit.Assert;
import org.junit.Test;


public class NoFirewallTestsIT extends AbstractJettyBasedIT {
  @Test
  public void shouldRespondOkForUrlThatExists() throws Exception {
    registerServlet(OkOnGetServlet.class, "/test");

    ContentResponse response = client.GET(String.format("http://localhost:%d/test", getJettyPort()));
    Assert.assertEquals(200, response.getStatus());

  }

  @Test
  public void shouldRespondNotFoundForUrlThatDoesNotExist() throws Exception {
    registerServlet(OkOnGetServlet.class, "/test");

    ContentResponse response = client.GET(String.format("http://localhost:%d/test123123", getJettyPort()));
    Assert.assertEquals(404, response.getStatus());

  }

}
