package net.secodo.besecurefirewall.jettybased;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.http.HttpClientTransportOverHTTP;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.junit.Test;


public class EmbeddedJettySupport {
  public Server startServer() throws Exception {
    final QueuedThreadPool serverThreads = new QueuedThreadPool();
    serverThreads.setName("integration-tests-server");

    final Server server = new Server(serverThreads);
    final ServletHandler servletHandler = new ServletHandler();
    final ServerConnector connector = new ServerConnector(server);

    server.addConnector(connector);
    server.setHandler(servletHandler);
    server.start();

    return server;

  }

  public HttpClient startClient() throws Exception {
    final QueuedThreadPool clientThreads = new QueuedThreadPool();
    clientThreads.setName("integrations-tests-client");

    final HttpClient client = new HttpClient(new HttpClientTransportOverHTTP(1), null);
    client.setExecutor(clientThreads);
    client.start();

    return client;
  }

  @Test
  public void shouldAbc() throws Exception {
    // given

    startServer();
    startClient();


    // when


    // then
  }

  //  @Test
  //  public void testStoppingClosesConnections() throws Exception {
  //    startServer();
  //    startClient();
  //
  //    String host = "localhost";
  //    int port = connector.getLocalPort();
  //    String path = "/";
  //    final HttpScheme httpScheme = HttpScheme.HTTP;
  //    Response response = client.GET(httpScheme + "://" + host + ":" + port + path);
  //    Assert.assertEquals(200, response.getStatus());
  //
  //    HttpDestinationOverHTTP destination = (HttpDestinationOverHTTP) client.getDestination(httpScheme.asString(),
  //      host,
  //      port);
  //    DuplexConnectionPool connectionPool = (DuplexConnectionPool) destination.getConnectionPool();
  //
  //    long start = System.nanoTime();
  //    HttpConnectionOverHTTP connection = null;
  //    while ((connection == null) && (TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - start) < 5)) {
  //      connection = (HttpConnectionOverHTTP) connectionPool.getIdleConnections().peek();
  //      TimeUnit.MILLISECONDS.sleep(10);
  //    }
  //    Assert.assertNotNull(connection);
  //
  //    String uri = destination.getScheme() + "://" + destination.getHost() + ":" + destination.getPort();
  //    client.getCookieStore().add(URI.create(uri), new HttpCookie("foo", "bar"));
  //
  //    client.stop();
  //
  //    Assert.assertEquals(0, client.getDestinations().size());
  //    Assert.assertEquals(0, connectionPool.getIdleConnectionCount());
  //    Assert.assertEquals(0, connectionPool.getActiveConnectionCount());
  //    Assert.assertFalse(connection.getEndPoint().isOpen());
  //  }

  public void disposeServerAndClient(Server server, HttpClient client) throws Exception {
    if (server != null) {
      server.stop();
      server = null;
    }
    if (client != null) {
      client.stop();
      client = null;
    }
  }

}
