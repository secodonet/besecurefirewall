package net.secodo.besecurefirewall.jettybased;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.FilterMapping;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.log.JavaUtilLog;
import org.eclipse.jetty.util.log.Log;
import org.junit.After;
import org.junit.Before;
import javax.servlet.Filter;
import javax.servlet.http.HttpServlet;


public class AbstractJettyBasedIT {
  private EmbeddedJettySupport jettySupport = new EmbeddedJettySupport();

  private Server server;
  protected HttpClient client;

  @Before
  public void startServerAndClient() throws Exception {
    Log.setLog(new JavaUtilLog());

    server = jettySupport.startServer();
    client = jettySupport.startClient();

  }

  protected int getJettyPort() {
    return ((ServerConnector) server.getConnectors()[0]).getLocalPort();
  }

  public void registerServlet(Class<? extends HttpServlet> servletClass, String path) {
    ((ServletHandler) server.getHandler()).addServletWithMapping(servletClass, path);
  }


  public void registerFilter(Filter filter, String path) {
    ((ServletHandler) server.getHandler()).addFilterWithMapping(new FilterHolder(filter), path, FilterMapping.DEFAULT);
  }

  @After
  public void stopServerAndClient() throws Exception {
    jettySupport.disposeServerAndClient(server, client);
  }


}
