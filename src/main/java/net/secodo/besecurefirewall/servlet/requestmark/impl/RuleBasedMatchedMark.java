package net.secodo.besecurefirewall.servlet.requestmark.impl;

import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.matcher.RuleMatcher;
import net.secodo.besecurefirewall.rule.matcher.impl.QueryStringMatcher;
import net.secodo.besecurefirewall.rule.matcher.impl.ServletPathMatcher;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;
import net.secodo.besecurefirewall.servlet.requestmark.MatchedMark;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contains data about rule which matched and action that was performed when match occurred.
 */
public class RuleBasedMatchedMark implements MatchedMark {


  private final DetectionPattern<?> detectionPattern;
  private final List<Location> locations;

  public RuleBasedMatchedMark(FirewallRule rule) {
    detectionPattern = rule.getDetectionPattern();

    List<Location> locations = determineLocations(rule);
    this.locations = Collections.unmodifiableList(locations);
  }

  @Override
  public DetectionPattern<?> getDetectionPattern() {
    return this.detectionPattern;
  }

  @Override
  public List<Location> getPatternLocations() {
    return this.locations;
  }

  private List<Location> determineLocations(FirewallRule rule) {
    final RuleMatcher matcher = rule.getMatcher();

    final List<Location> locations = new ArrayList<Location>();
    locations.add(Location.REQUEST);

    if (matcher instanceof ServletPathMatcher) {
      // TODO:
    } else if (matcher instanceof QueryStringMatcher) {
      // TODO:
    }


    return locations;
  }
}
