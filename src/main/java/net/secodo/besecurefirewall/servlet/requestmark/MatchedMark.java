package net.secodo.besecurefirewall.servlet.requestmark;

import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;

import java.util.List;

public interface MatchedMark {

  /**
   * Returns detection pattern which was matched against the request. The exact pattern which matched may be obtained
   * by call to {@link DetectionPattern#getPatternString()}
   *
   * @return the detection pattern which matched
   */
  DetectionPattern<?> getDetectionPattern();

  /**
   * Returns the locations where certain pattern defined for some rules was found. For example if the pattern was
   * matched against "query parameter name" there will be 4 marks available
   * inside {@link RequestMarks}:  {@link Location#QUERY_PARAMETER_NAME}, {@link Location#QUERY_STRING},
   * {@link Location#URL} and {@link Location#REQUEST}
   *
   * @return the locations to which this mark refers to. This corresponds to where the detection pattern was
   * found.
   */
  List<Location> getPatternLocations();

  enum Location {
    /**
     * Indicates that a signature was matched in query parameter name
     */
    QUERY_PARAMETER_NAME,

    /**
     * Indicates that a signature was matched in query parameter value
     */
    QUERY_PARAMETER_VALUE,

    /**
     * Indicates that a signature was matched in query string. This might mean that the signature spread over the
     * parameter name or parameter value. For example if query string is "?foo=bar&baz=some" and the detection pattern
     * is "r&baz" it matches in query string and not in individual query param key-value pair.
     */
    QUERY_STRING,

    /**
     * Indicates that a signature was matched in servlet path part. The "part" means the servlet path is split by
     * "/" character. For example URI "/some/foo/bar" consists of three parts: "some", "foo", "bar".
     */
    SERVLET_PATH_PART,

    /**
     * Indicates that a signature was matched in servlet path. This means that the detection pattern contained pattern
     * including "/" character and spread over more than one path element. For example if URI is "/some/foo/bar" and
     * detection pattern is "me/foo" than it does not match any individual part.
     */
    SERVLET_PATH,

    /**
     * Indicates none of above, or combination of above. For example detected pattern spread over a servlet path
     * and a query string, was present in host, port (if matcher considered these) etc.
     *
     */
    URL,

    /**
     * Indicates none of above, or combination of above. For example pattern was matched in cookies or elsewhere
     * defined by custom matchers. An example is {@link net.secodo.besecurefirewall.rule.matcher.impl.IpAddressMatcher}
     * which does not match against url.
     *
     * NOTE: in future versions more location may come, such as cookies
     */
    REQUEST

  }
}
