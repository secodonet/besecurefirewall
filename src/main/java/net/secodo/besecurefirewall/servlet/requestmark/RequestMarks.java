package net.secodo.besecurefirewall.servlet.requestmark;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contains information about rules and signatures which matched current request. Marks are set by
 * {@link net.secodo.besecurefirewall.action.impl.AcknowledgeFirewallAction}.
 */
public class RequestMarks {

  private List<MatchedMark> marks;

  public RequestMarks() {
    this.marks = new ArrayList<MatchedMark>();
  }

  public void addMark(MatchedMark mark) {
    this.marks.add(mark);
  }

  /**
   * Returns marks in safe - unmodifiable list.
   * @return list containing all marks
   */
  public List<MatchedMark> getMarks() {
    return Collections.unmodifiableList(marks);
  }

  public List<MatchedMark> getModifiableMarks() {
    return Collections.unmodifiableList(marks);
  }


}
