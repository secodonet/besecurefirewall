package net.secodo.besecurefirewall.servlet.wrapper;

import javax.servlet.ServletResponse;


public class ServletResponseWrapper {
  private ServletResponse servletResponse;

  public ServletResponseWrapper(ServletResponse servletResponse) {
    this.servletResponse = servletResponse;
  }

  public ServletResponse getServletResponse() {
    return servletResponse;
  }

  public void replaceResponse(ServletResponse servletResponse) {
    this.servletResponse = servletResponse;
  }

}
