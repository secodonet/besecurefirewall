package net.secodo.besecurefirewall;

import net.secodo.besecurefirewall.action.ActionResult;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.config.ConfigReader;
import net.secodo.besecurefirewall.logging.FirewallLogger;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.callback.FirewallRuleCallback;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;


/**
 * Main class defining the firewall. Holds an instance of {@link BeSecureFirewallConfig} to gain access to all the rules
 * defining this firewall.
 *
 * In should be installed as a standard HttpFilter into the web application. The filter takes one init parameter "configfile"
 * which defines the config file located on classpath with all the rules defining this firewall as well as firewall default
 * policy.
 *
 * Config can be build manually and applied to the firewall at runtime via call to {@link #setConfig(BeSecureFirewallConfig)}
 *
 */
public class BeSecureFirewall implements Filter {
  private static final String CONFIG_FILE = "configfile";
  private FirewallLogger logger;

  private BeSecureFirewallConfig config;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    logger = FirewallLogger.getLogger();

    if (config == null) { // custom ConfigReader was passed

      final String configFileName = filterConfig.getInitParameter(CONFIG_FILE);

      if (configFileName == null) {
        logger.warn(
          "Init parameter: " + CONFIG_FILE + " is missing. " + BeSecureFirewall.class.getSimpleName() +
          " will not be initialized");

        this.config = BeSecureFirewallConfig.createDirtyConfig();
        logger.info("BeSecureFirewall initialization not finished. No config found.");

      } else {
        final ConfigReader configReader = new ConfigReader();
        this.config = configReader.readConfigFromClassPathResource(configFileName);
        logger.info(
          "BeSecureFirewall initialized with " + this.config.getFirewallRules().size() +
          " firewall rules read from config file: " + configFileName + ".");
      }
    }

  }


  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
                throws IOException, ServletException {
    if (config.isDirty()) {
      filterChain.doFilter(servletRequest, servletResponse);
      return;
    }


    final ServletRequestWrapper requestWrapper = new ServletRequestWrapper(servletRequest);
    final ServletResponseWrapper responseWrapper = new ServletResponseWrapper(servletResponse);

    for (FirewallRule rule : config.getFirewallRules()) {
      if (rule.getMatcher().matches(requestWrapper, rule.getDetectionPattern())) {
        for (FirewallRuleCallback callback : rule.getOnMatchCallbacks()) {
          callback.call(config, requestWrapper, rule);
        }

        final ActionResult actionResult = rule.getAction().handleRequest(config, requestWrapper, responseWrapper,
          filterChain);
        if (actionResult == null) {
          logger.error("Rule: " + rule.getName() + " returned null action. Processing of other rules will stop");
          return;
        }

        if (actionResult.isRequestHandled()) {
          return;
        }

      }
    }

    config.getDefaultPolicyAction().handleRequest(config, requestWrapper, responseWrapper, filterChain);
  }

  public BeSecureFirewallConfig getConfig() {
    return config;
  }

  @Override
  public void destroy() {
  }

  public void setConfig(BeSecureFirewallConfig config) {
    this.config = config;
  }
}
