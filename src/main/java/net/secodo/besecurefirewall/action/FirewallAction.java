package net.secodo.besecurefirewall.action;

import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;


/**
 * Represents the action which should happen when a
 * {@link FirewallRule} matched the request.
 *
 * It's also used as a firewall policy
 *
 */
public interface FirewallAction {
  /**
   * Performs an action on given request.
   *
   * Can result in processing the request, modifying it or not processing in the filterChain. Can wrap the request,
   * set some response code, request headers etc.
   *
   * Returns {@link ActionResult} which can be interpreted by firewall. Basing on the result the firewall may decide to stop processing
   * the chain of firewall rules or to continue the execution.
   *
   *
   * @param config contains configuration of the firewall which called this action. NOTE: when creating custom
   *               action care should be taken not to pass whole config as request attribute as it might be
   *               intercepted and modified (maliciously) which is usually not expected behaviour
   * @param requestWrapper  the actual servlet request which for current request which is normally passed later to application
   * @param responseWrapper the actual servlet response which for current request which is normally passed later to application
   * @param filterChain the filter chain which is handling the current request
   * @throws IOException in case IO problems
   * @throws ServletException in case other problems with processing
   *
   * @return and action result which can be interpreted by firewall.
   */
  ActionResult handleRequest(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper, ServletResponseWrapper responseWrapper,
                             FilterChain filterChain) throws IOException, ServletException;
}
