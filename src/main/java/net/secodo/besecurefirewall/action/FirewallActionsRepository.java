package net.secodo.besecurefirewall.action;

import net.secodo.besecurefirewall.action.impl.AcknowledgeFirewallAction;
import net.secodo.besecurefirewall.action.impl.AllowFirewallAction;
import net.secodo.besecurefirewall.action.impl.experimental.DropFirewallAction;
import net.secodo.besecurefirewall.action.impl.DenyFirewallAction;
import java.util.HashMap;
import java.util.Map;


/**
 * Stores and provides all available parsers of detection patterns.
 *
 */
public class FirewallActionsRepository {
  private Map<String, FirewallAction> registeredActions = new HashMap<String, FirewallAction>();

  public interface Signatures {
    String DENY = "DENY";
    String ALLOW = "ALLOW";
    String DROP = "DROP"; // this is experimental and may be removed
    String ACKNOWLEDGE = "ACKNOWLEDGE";

  }


  public FirewallActionsRepository() {
    // register default actions
    final DenyFirewallAction denyAction = new DenyFirewallAction();
    final DropFirewallAction dropAction = new DropFirewallAction(); // this is experimental and may be removed
    final AllowFirewallAction allowAction = new AllowFirewallAction();
    final AcknowledgeFirewallAction acknowledgeAction = new AcknowledgeFirewallAction();

    registeredActions.put(Signatures.DENY, denyAction);
    registeredActions.put(Signatures.DROP, dropAction); // this is experimental and may be removed
    registeredActions.put(Signatures.ALLOW, allowAction);
    registeredActions.put(Signatures.ACKNOWLEDGE, acknowledgeAction);
  }

  /**
   * Allows to manually register rule firewall action. Given action will be registered in this repository with
   * configSignature as defined by given config signature.
   *
   * @param configSignature the identifier under which given action will be registered in this repository
   * @param firewallAction the firewall action to register in this instance of repository
   * @return this instance of repository
   */
  public FirewallActionsRepository registerAction(String configSignature, FirewallAction firewallAction) {
    registeredActions.put(configSignature, firewallAction);

    return this;
  }

  /**
   * Returns null if there is no registered action with given config signature
   * @param configSignature the identifier of the action used in config file
   * @return
   */
  /**
   * Returns an instance of {@link FirewallAction} registered in this repository with given config signature
   *
   * @see #registerAction(String, FirewallAction)
   * @param configSignature the identifier of the action used in config file
   * @return an instance of {@link FirewallAction} or null if there is no action registered with given configSignature
   */
  public FirewallAction getBySignature(String configSignature) {
    return registeredActions.get(configSignature);
  }
}
