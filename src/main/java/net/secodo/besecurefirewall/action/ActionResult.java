package net.secodo.besecurefirewall.action;

/**
 * The result of executing an action on request. Currently contains only status saying whether the request was handled by the
 * action or now.
 *
 * See the comment for method: {@see FirewallAction#handleRequest(ServletRequest, ServletResponse, FilterChain)}
 */
public class ActionResult {
  private final boolean requestHandled;

  public static final ActionResult REQUEST_HANDLED = new ActionResult(true);
  public static final ActionResult REQUEST_NOT_HANDLED = new ActionResult(false);

  public ActionResult(boolean requestHandled) {
    this.requestHandled = requestHandled;
  }

  public boolean isRequestHandled() {
    return requestHandled;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if ((o == null) || (getClass() != o.getClass())) {
      return false;
    }

    ActionResult that = (ActionResult) o;

    return requestHandled == that.requestHandled;
  }

  @Override
  public int hashCode() {
    return (requestHandled ? 1 : 0);
  }
}
