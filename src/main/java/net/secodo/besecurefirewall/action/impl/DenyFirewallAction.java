package net.secodo.besecurefirewall.action.impl;

import net.secodo.besecurefirewall.action.ActionResult;
import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import javax.servlet.FilterChain;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;


/**
 * This action does not pass the request to the application but instead responds with HTTP 403 Forbidden.
 *
 */
public class DenyFirewallAction implements FirewallAction {
  @Override
  public ActionResult handleRequest(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper, ServletResponseWrapper responseWrapper,
                                    FilterChain filterChain) {
    final ServletResponse servletResponse = responseWrapper.getServletResponse();

    if (servletResponse instanceof HttpServletResponse) {
      ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    return ActionResult.REQUEST_HANDLED;
  }

}
