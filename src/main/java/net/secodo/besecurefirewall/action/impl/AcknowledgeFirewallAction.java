package net.secodo.besecurefirewall.action.impl;

import net.secodo.besecurefirewall.action.ActionResult;
import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;


/**
 * This action does not process the request it just allows to execute custom callback methods when request is matched.
 *
 * This is useful for example in case one want to detect possible attack and notify about it, rather than explicitly
 * blocking the request as it comes.
 *
 * NOTE: This action causes next firewall rules to be evaluated, so one of the later rules can still allow or block
 * the request.
 */
public class AcknowledgeFirewallAction implements FirewallAction {
  /**
   * This action does not handle the request. The request is passed further for another rules for
   * evaluation.
   *
   */
  @Override
  public ActionResult handleRequest(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper,
                                    ServletResponseWrapper responseWrapper, FilterChain filterChain)
      throws IOException, ServletException {
    // NOTE: no handling of the request
    return ActionResult.REQUEST_NOT_HANDLED;
  }
}
