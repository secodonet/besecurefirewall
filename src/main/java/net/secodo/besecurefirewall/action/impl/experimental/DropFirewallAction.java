package net.secodo.besecurefirewall.action.impl.experimental;

import net.secodo.besecurefirewall.action.ActionResult;
import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import javax.servlet.FilterChain;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;


/**
 * This is experimental action. It sends with HTTP 403 Forbidden and additional sends the Conection:close header.
 *
 */
public class DropFirewallAction implements FirewallAction {
  @Override
  public ActionResult handleRequest(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper, ServletResponseWrapper responseWrapper,
                                    FilterChain filterChain) {
    final ServletResponse servletResponse = responseWrapper.getServletResponse();

    if (servletResponse instanceof HttpServletResponse) {
      ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_FORBIDDEN);
      ((HttpServletResponse) servletResponse).setHeader("Connection", "close");
    }
    // nothing to do

    return ActionResult.REQUEST_HANDLED;
  }

}
