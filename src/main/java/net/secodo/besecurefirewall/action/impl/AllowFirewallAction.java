package net.secodo.besecurefirewall.action.impl;

import net.secodo.besecurefirewall.action.ActionResult;
import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import net.secodo.besecurefirewall.servlet.wrapper.ServletResponseWrapper;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;


/**
 * This action allows the defined request to pass through the firewall.
 *
 * This action can be used to whitelist some urls (resulting in such requests not beeing analized by the rest of firewall rules).
 * As an example certain IP addresses can be allowed to post requests which are not allowed by other hosts.
 */
public class AllowFirewallAction implements FirewallAction {
  @Override
  public ActionResult handleRequest(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper, ServletResponseWrapper responseWrapper,
                                    FilterChain filterChain) throws IOException, ServletException {
    // ok.. go on
    filterChain.doFilter(requestWrapper.getServletRequest(), responseWrapper.getServletResponse());

    return ActionResult.REQUEST_HANDLED;
  }


}
