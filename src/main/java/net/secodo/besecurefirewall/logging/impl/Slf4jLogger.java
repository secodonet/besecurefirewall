package net.secodo.besecurefirewall.logging.impl;

import net.secodo.besecurefirewall.logging.FirewallLogger;
import org.slf4j.Logger;


public class Slf4jLogger extends FirewallLogger {
  private final Logger logger;

  public Slf4jLogger(Logger logger) {
    this.logger = logger;
  }

  @Override
  public void info(String message) {
    logger.info(message);
  }

  @Override
  public void warn(String message) {
    logger.warn(message);
  }

  @Override
  public void warn(String message, Exception e) {
    logger.warn(message, e);
  }

  @Override
  public void error(String message) {
    logger.error(message);
  }

  @Override
  public void error(String message, Exception e) {
    logger.error(message, e);
  }
}
