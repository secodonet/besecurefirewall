package net.secodo.besecurefirewall.logging.impl;

import net.secodo.besecurefirewall.logging.FirewallLogger;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JavaLoggingApiLogger extends FirewallLogger {
  private final Logger logger;

  public JavaLoggingApiLogger(Logger logger) {
    this.logger = logger;
  }

  @Override
  public void info(String message) {
    logger.info(message);
  }

  @Override
  public void warn(String message) {
    logger.warning(message);
  }

  @Override
  public void warn(String message, Exception e) {
    logger.log(Level.WARNING, message, e);
  }

  @Override
  public void error(String message) {
    logger.log(Level.SEVERE, message);
  }

  @Override
  public void error(String message, Exception e) {
    logger.log(Level.SEVERE, message, e);
  }
}
