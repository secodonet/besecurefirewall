package net.secodo.besecurefirewall.logging;

import net.secodo.besecurefirewall.logging.impl.JavaLoggingApiLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.secodo.besecurefirewall.BeSecureFirewall;
import net.secodo.besecurefirewall.logging.impl.Slf4jLogger;
import java.lang.reflect.Method;
import java.util.logging.Level;


public abstract class FirewallLogger {
  private static final String LOGGER_NAME = "BeSecureFirewall";

  private static FirewallLogger instance;
  private boolean initialized;

  private FirewallLogger logger;

  public static FirewallLogger getLogger() {
    if ((instance == null) || !instance.initialized) {
      // try initialized slf4j
      java.util.logging.Logger javaApiLogger = java.util.logging.Logger.getLogger(LOGGER_NAME);

      try {
        final Class<LoggerFactory> loggerFactoryClass = createSlf4jLoggerFactoryClass();
        final Method getLoggerMethod = loggerFactoryClass.getMethod("getLogger", String.class);

        final Logger slf4jLogger = (Logger) getLoggerMethod.invoke(loggerFactoryClass, LOGGER_NAME);

        if (!slf4jLogger.getClass().getSimpleName().contains("NOPLogger") &&
            !slf4jLogger.getClass().getSimpleName().contains("NOP")) {
          instance = new Slf4jLogger(slf4jLogger);

        }

        if (instance == null) {
          javaApiLogger.info(
            BeSecureFirewall.class.getSimpleName() + " could not initialize slf4j logger. " +
            ((slf4jLogger != null) ? ("Logger class was: " + slf4jLogger.getClass() + ". ") : "") +
            "Will use java logging api.");
        }


      } catch (Exception e) {
        javaApiLogger.log(Level.INFO,
          "Unable to initialize " + BeSecureFirewall.class.getSimpleName() +
          " logger using slf4j. Will use java logging api. Exception was: " + e.getClass() + " (" + e.getMessage() +
          ((e.getCause() != null) ? ("(" + e.getCause().getMessage() + ")") : "") + ")");
      }

      if (instance == null) {
        instance = new JavaLoggingApiLogger(javaApiLogger);
      }

      instance.initialized = true; // if slf4jLogger
    }

    return instance;
  }

  @SuppressWarnings("unchecked")
  private static Class<LoggerFactory> createSlf4jLoggerFactoryClass() throws ClassNotFoundException {
    return (Class<LoggerFactory>) Class.forName("org.slf4j.LoggerFactory");
  }

  public abstract void info(String message);

  public abstract void warn(String message);

  public abstract void warn(String message, Exception e);

  public abstract void error(String message);

  public abstract void error(String message, Exception e);


}
