package net.secodo.besecurefirewall.config;

import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.logging.FirewallLogger;
import net.secodo.besecurefirewall.rule.buillder.IncompleteRuleException;
import net.secodo.besecurefirewall.rule.callback.FirewallCallbacksRepository;
import net.secodo.besecurefirewall.rule.matcher.RuleMatcher;
import net.secodo.besecurefirewall.BeSecureFirewall;
import net.secodo.besecurefirewall.action.FirewallActionsRepository;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.buillder.RuleBuilder;
import net.secodo.besecurefirewall.rule.buillder.RuleBuilderException;
import net.secodo.besecurefirewall.rule.buillder.RuleParsingException;
import net.secodo.besecurefirewall.rule.matcher.RuleMatchersRepository;
import net.secodo.besecurefirewall.rule.callback.FirewallRuleCallback;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPatternParser;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPatternParsersRepository;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import static net.secodo.besecurefirewall.action.FirewallActionsRepository.Signatures.ALLOW;


public class ConfigReader {
  private final FirewallLogger logger;
  private static final String CONFIG_COMMAND_ACTION = "action";
  private static final String CONFIG_COMMAND_MATCHER = "matcher";
  private static final String CONFIG_COMMAND_ON_MATCH_CALLBACKS = "onmatchcallbacks";
  private static final String CONFIG_COMMAND_PATTERN = "pattern";
  private static final String CONFIG_COMMAND_PATTERN_PARSER = "patternparser";

  private static final String CALLBACK_CONFIG_COMMAND_REGISTER_BY_CLASS = "class";
  private static final String CALLBACK_CONFIG_IN_RULE_SIGNATURE_SEPARATOR = ","; // separates multiple callbacks

  public static final String MATCHERS_WILDCARD_CONFIG_KEY = "matchers.wildcard.string";
  public static final String POLICY_DEFAULT_ACTION_CONIFG_KEY = "policy.default.action";

  private Class<ConfigReader> resourceLoadingClass = ConfigReader.class; // convenience field which can be mocked up in test


  public ConfigReader() {
    logger = FirewallLogger.getLogger();
  }


  public BeSecureFirewallConfig readConfigFromClassPathResource(final String classPathFile) {
    final FirewallCallbacksRepository callbacksRepository = new FirewallCallbacksRepository();
    final FirewallActionsRepository actionsRepository = new FirewallActionsRepository();
    final RuleMatchersRepository matchersRepository = new RuleMatchersRepository();
    final DetectionPatternParsersRepository detectionPatternParsersRepository = new DetectionPatternParsersRepository();


    final BeSecureFirewallConfig config = new BeSecureFirewallConfig(matchersRepository,
      callbacksRepository,
      detectionPatternParsersRepository,
      actionsRepository);


    InputStream configFileStream = resourceLoadingClass.getResourceAsStream(classPathFile);

    if (configFileStream == null) {
      configFileStream = resourceLoadingClass.getResourceAsStream("/" + classPathFile);
    }


    if (configFileStream == null) {
      logWarning("Unable to load configuration from classpath config file.", classPathFile);

      config.setDirty(true);
      return config;

    }


    final Map<String, RuleBuilder> rules = new LinkedHashMap<String, RuleBuilder>();

    try {
      final LinkedHashMap<String, String> orderedProperties = readPropertiesInOrderFrom(configFileStream);

      // TODO: parse requestmarks.attribute.name

      parseAndRegisterCallbacks(classPathFile, orderedProperties, callbacksRepository);

      readRules(classPathFile,
        rules,
        orderedProperties,
        actionsRepository,
        matchersRepository,
        detectionPatternParsersRepository,
        callbacksRepository);

      buildRules(classPathFile, rules, config);

      readMatchersWildcard(orderedProperties, config);
      readDefaultFirewallPolicyAction(orderedProperties, config, actionsRepository);


    } catch (Exception e) {
      logWarning("Unable to load configuration from classpath config file.", classPathFile, e);

      config.setDirty(true);
      return config;
    } finally {
      try {
        configFileStream.close();
      } catch (IOException e) {
        logWarning("Problem while closing config file stream for file: " + classPathFile + ". ", classPathFile, e);
      }
    }


    return config;
  }

  private void readMatchersWildcard(LinkedHashMap<String, String> properties,
                                    BeSecureFirewallConfig initializedConfig) {
    if (properties.containsKey(MATCHERS_WILDCARD_CONFIG_KEY)) {
      initializedConfig.setMatchersWildcard(properties.get(MATCHERS_WILDCARD_CONFIG_KEY));
    }
  }

  private void readDefaultFirewallPolicyAction(LinkedHashMap<String, String> properties,
                                               BeSecureFirewallConfig initializedConfig,
                                               FirewallActionsRepository actionsRepository) {
    if (properties.containsKey(POLICY_DEFAULT_ACTION_CONIFG_KEY)) {
      final FirewallAction defaultFirewallAction = actionsRepository.getBySignature(
        properties.get(POLICY_DEFAULT_ACTION_CONIFG_KEY));

      if (defaultFirewallAction != null) {
        initializedConfig.setDefaultPolicyAction(defaultFirewallAction);
      } else {
        logger.warn(
          "Unsupported firewall action: " + properties.get(POLICY_DEFAULT_ACTION_CONIFG_KEY) +
            " defined as default firewall policy action");
      }


    } else {
      initializedConfig.setDefaultPolicyAction(actionsRepository.getBySignature(ALLOW));
    }
  }

  private void parseAndRegisterCallbacks(String classPathFile, LinkedHashMap<String, String> properties,
                                         FirewallCallbacksRepository callbacksRepository) {
    // callback.COUNTER_CALLBACK.class = net.secodo.besecurefirewall.config.MatchesCounterCallback


    for (Map.Entry<String, String> entry : properties.entrySet()) {
      final String key = entry.getKey();
      final String value = (entry.getValue() != null) ? entry.getValue() : null;


      if (key.startsWith("callback.")) {
        StringTokenizer tokenizer = new StringTokenizer(key, ".");
        if (tokenizer.countTokens() != 3) { // 1 - "rule", 2 - "rule name", 3 - rule command
          logger.warn("Incorrect definition of firewall callback: " + key + " Callback will not be included.");
        } else {
          tokenizer.nextToken(); // "rule" String

          String callbackName = tokenizer.nextToken();
          String callbackCommand = tokenizer.nextToken();

          if (CALLBACK_CONFIG_COMMAND_REGISTER_BY_CLASS.equals(callbackCommand)) {
            try {
              final Class<?> clazz = Class.forName(value);
              final Object callbackInstance = clazz.newInstance();
              if (!(callbackInstance instanceof FirewallRuleCallback)) {
                logWarning(
                  "Class " + value + " defined by callback " + callbackName + " is not instance of: " +
                    FirewallRuleCallback.class.getSimpleName(),
                  classPathFile);
              } else {
                callbacksRepository.registerCallback(callbackName, (FirewallRuleCallback) callbackInstance);
              }

            } catch (Exception e) {
              logWarning(
                "Unable to instantiate class " + value + " defined by callback " + callbackName,
                classPathFile,
                e);
            }

          } else {
            logWarning(
              "Unsupported firewall callback property definition: " + callbackCommand + " for rule: " + callbackName,
              classPathFile);
          }

        }

      }

    }
  }


  private void buildRules(String classPathFile, Map<String, RuleBuilder> rules,
                          BeSecureFirewallConfig initializedConfig) {
    for (Map.Entry<String, RuleBuilder> entry : rules.entrySet()) {
      final RuleBuilder builder = entry.getValue();


      try {
        FirewallRule rule = builder.build();
        initializedConfig.addRule(rule);

      } catch (IncompleteRuleException e) {
        logger.warn(
          "Incomplete rule definition with name: " + e.getRuleName() + ". Missing field: " + e.getMissingField());
      } catch (RuleParsingException e) {
        logger.warn(
          "Invalid rule definition with name: " + e.getRuleName() + ". Unable to parse the pattern: " + e.getPattern() +
            " with parser: " +
            ((e.getParser() != null) ? e.getParser().getClass() : null) + " for rule: " +
            e.getRuleName());

      } catch (RuleBuilderException e) {
        logWarning(
          "Exception while build rule: " + e.getRuleName(),
          classPathFile,
          e);
      }

    }
  }


  private void readRules(String classPathFile, Map<String, RuleBuilder> rules, LinkedHashMap<String, String> properties,
                         FirewallActionsRepository actionsRepository, RuleMatchersRepository matchersRepository,
                         DetectionPatternParsersRepository patternParsersRepository,
                         FirewallCallbacksRepository callbacksRepository) {
    // rule.drop0Char.matcher = URL_MATCHER
    // rule.drop0Char.onmatchcallback=COUNTER_CALLBACK
    // rule.drop0Char.pattern = \u0000
    // rule.drop0Char.action = DROP


    for (Map.Entry<String, String> entry : properties.entrySet()) {
      final String key = entry.getKey();
      final String value = (entry.getValue() != null) ? entry.getValue() : null;


      if (key.startsWith("rule.")) {
        StringTokenizer tokenizer = new StringTokenizer(key, ".");
        if (tokenizer.countTokens() != 3) { // 1 - "rule", 2 - "rule name", 3 - rule command
          logger.warn("Incorrect definition of rule: " + key + " Rule will not be included.");
        } else {
          tokenizer.nextToken(); // "rule" String

          String ruleName = tokenizer.nextToken();
          String ruleCommand = tokenizer.nextToken();

          final RuleBuilder ruleBuilder;
          if (!rules.containsKey(ruleName)) {
            ruleBuilder = RuleBuilder.init().withName(ruleName);
            rules.put(ruleName, ruleBuilder);
          } else {
            ruleBuilder = rules.get(ruleName);
          }

          if (CONFIG_COMMAND_ACTION.equals(ruleCommand)) {
            String configSignature = (value != null) ? value.trim() : null;

            final FirewallAction firewallAction = actionsRepository.getBySignature(configSignature);

            if (firewallAction != null) {
              ruleBuilder.withAction(firewallAction);
            } else {
              logWarning("Unsupported firewall action: " + value + " for rule: " + ruleName, classPathFile);
            }

          } else if (CONFIG_COMMAND_MATCHER.equals(ruleCommand)) {
            String configSignature = (value != null) ? value.trim() : null;

            final RuleMatcher ruleMatcher = matchersRepository.getByConfigSignature(configSignature);
            if (ruleMatcher != null) {
              ruleBuilder.withMatcher(ruleMatcher);
            } else {
              logWarning("Unsupported firewall matcher: " + value + " for rule: " + ruleName, classPathFile);
            }
          } else if (CONFIG_COMMAND_PATTERN_PARSER.equals(ruleCommand)) {
            String configSignature = (value != null) ? value.trim() : null;

            final DetectionPatternParser<?> patternParser = patternParsersRepository.getByConfigSignature(
              configSignature);
            if (patternParser != null) {
              ruleBuilder.withDetectionPatternParser(patternParser);
            } else {
              logWarning("Unsupported firewall detection pattern parser: " + value + " for rule: " + ruleName,
                classPathFile);
            }
          } else if (CONFIG_COMMAND_PATTERN.equals(ruleCommand)) {
            ruleBuilder.withPattern(value);

          } else if (CONFIG_COMMAND_ON_MATCH_CALLBACKS.equals(ruleCommand)) {

            String[] callbacksSignatures = value.split(CALLBACK_CONFIG_IN_RULE_SIGNATURE_SEPARATOR);

            for (String signature : callbacksSignatures) {
              final FirewallRuleCallback firewallCallback = callbacksRepository.getByConfigSignature(signature);
              if (firewallCallback == null) {
                logWarning("Callback with identifier " + value + " defined by " + ruleCommand + " for rule: " +
                  ruleName + " was not registered and can not be found", classPathFile);
              } else {
                ruleBuilder.withOnMatchCallback(firewallCallback);
              }
            }

          } else {
            logWarning("Unsupported firewall rule property definition: " + ruleCommand + " for rule: " + ruleName,
              classPathFile);
          }

        }

      }

    }
  }

  private LinkedHashMap<String, String> readPropertiesInOrderFrom(InputStream propertiesFileInputStream)
    throws IOException {
    if (propertiesFileInputStream == null) {
      return new LinkedHashMap<String, String>(0);
    }

    LinkedHashMap<String, String> orderedProperties = new LinkedHashMap<String, String>();

    final Properties properties = new Properties(); // use only as a parser
    final BufferedReader reader = new BufferedReader(new InputStreamReader(propertiesFileInputStream));

    String rawLine = reader.readLine();

    while (rawLine != null) {
      final ByteArrayInputStream lineStream = new ByteArrayInputStream(rawLine.getBytes("ISO-8859-1"));
      properties.load(lineStream); // load only one line, so there is not problem with messing up the order


      final Enumeration<?> propertyNames = properties.<String>propertyNames();

      if (propertyNames.hasMoreElements()) { // need to check because there can be empty or not parsable line for example

        final String parsedKey = (String) propertyNames.nextElement();
        final String parsedValue = properties.getProperty(parsedKey);

        orderedProperties.put(parsedKey, parsedValue);
        properties.clear(); // make sure next iteration of while loop does not access current property
      }

      rawLine = reader.readLine();
    }

    return orderedProperties;

  }


  private void logWarning(String warningMessage, String classPathFile, Exception e) {
    if (e != null) {
      logger.warn(
        warningMessage + ". " + BeSecureFirewall.class.getSimpleName() +
          " may not work as expected. Config file on classpath was: " +
          classPathFile,
        e);

    } else {
      logger.warn(
        warningMessage + " " + BeSecureFirewall.class.getSimpleName() +
          " may not work as expected. Config file on classpath was: " +
          classPathFile);
    }
  }

  private void logWarning(String warningMessage, String classPathFile) {
    logWarning(warningMessage, classPathFile, null);
  }
}
