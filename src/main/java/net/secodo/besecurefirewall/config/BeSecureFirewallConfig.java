package net.secodo.besecurefirewall.config;

import net.secodo.besecurefirewall.BeSecureFirewall;
import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.action.FirewallActionsRepository;
import net.secodo.besecurefirewall.action.impl.AllowFirewallAction;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.callback.FirewallCallbacksRepository;
import net.secodo.besecurefirewall.rule.matcher.RuleMatchersRepository;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPatternParsersRepository;
import java.util.ArrayList;
import java.util.List;
import static java.util.Collections.unmodifiableList;


/**
 * Represents the configuration of the firewall.
 *
 * Configuration is normally read from a class path file, defined by "configfile" init-param of the
 * {@link BeSecureFirewall} filter, but can also be build manually.
 *
 * Config can be modified at runtime, by adding a rules via {@link #addRule(FirewallRule)} or {@link #addRules(FirewallRule...)}.
 * Rules can be cleaned up by calling {@link #clearRules()}.
 *
 * The config remains in state either dirty or not. Normally every config should be dirty (otherwise the firewall
 * does not do anything). However not dirty config can be used to disable the firewall for some time at runtime.
 * Also when the config is read automatically from classpath and there is a problem while parsing the configuration file or
 * the files is not found, a config can be created as "uninitialized" instance.
 *
 * Config stores the chain of rules ({@link FirewallRule}). Rules added earlier takes precedence over the rules added later.
 * First matched rule wins, meaning that after the action associated with given rule is executed, other rules are not
 * evaluated.
 *
 * If none of the rules matches the default action is executed. The default action is defined by firewall policy
 * by calling {@link #setDefaultPolicyAction(FirewallAction)}. The default policy, unless modified, allows the request to be
 * processed.
 *
 */
public class BeSecureFirewallConfig {
  /**
   * Constant which can be referenced from outside to find out what is the default name for "marks" request
   * attribute.
   * @see net.secodo.besecurefirewall.servlet.requestmark.RequestMarks
   */
  public static final String DEFAULT_MARKS_REQUEST_ATTRIBUTE_NAME = "BE_SECURE_FIREWALL_MARKS";

  private FirewallAction defaultPolicyAction = new AllowFirewallAction();
  private String matchersWildcard = null; // NOT YET SUPPORTED
  private String requestMarksAttributeName = DEFAULT_MARKS_REQUEST_ATTRIBUTE_NAME;

  private List<FirewallRule> rules = new ArrayList<FirewallRule>();

  /**
   * the parameter says wheather the config is dirty or not. In most situations this flag is false. It
   * is set to true when there is a problem with reading the firewall copnfig from classpath file, If config
   * is falgged as dirty than firewall does not evaluate it's rules, so it can be used to disable the
   * firewall for some time at runtime.
   *
   */
  private boolean dirty = false;

  private final RuleMatchersRepository matchersRepository;
  private final FirewallCallbacksRepository callbacksRepository;
  private final DetectionPatternParsersRepository detectionPatternParsersRepository;
  private final FirewallActionsRepository actionsRepository;


  /**
   * Creates new config with default handlers (for example: actions) registered inside
   */
  public BeSecureFirewallConfig() {
    this(new RuleMatchersRepository(),
      new FirewallCallbacksRepository(),
      new DetectionPatternParsersRepository(),
      new FirewallActionsRepository());
  }

  /**
   * Returns new instance of the config.
   *
   * @param matchersRepository contains all registered instances of {@link net.secodo.besecurefirewall.rule.matcher.RuleMatcher}
   * @param callbacksRepository contains all registered instances of {@link net.secodo.besecurefirewall.rule.callback.FirewallRuleCallback}
   * @param detectionPatternParsersRepository contains all registered instances of {@link DetectionPatternParsersRepository}
   * @param actionsRepository contains all registered instances of {@link FirewallActionsRepository}
   */
  public BeSecureFirewallConfig(RuleMatchersRepository matchersRepository,
                                FirewallCallbacksRepository callbacksRepository,
                                DetectionPatternParsersRepository detectionPatternParsersRepository,
                                FirewallActionsRepository actionsRepository) {
    this.matchersRepository = matchersRepository;
    this.callbacksRepository = callbacksRepository;
    this.detectionPatternParsersRepository = detectionPatternParsersRepository;
    this.actionsRepository = actionsRepository;
  }

  public static BeSecureFirewallConfig createDirtyConfig() {
    final BeSecureFirewallConfig config = new BeSecureFirewallConfig();
    config.setDirty(true);
    return config;
  }


  /**
   * Add rule to this firewall. The order of adding rules matters. The rules added earlier have higher priority.
   *
   * @param firewallRule the rule which will be added to the end of the rules chain (so that will be evaluted after the rules
   *                     added earlier)
   */
  public void addRule(FirewallRule firewallRule) {
    rules.add(firewallRule);
  }

  /**
   * Add multiple rules to this firewall. The order of adding rules matters. The rules added earlier have higher priority.
   *
   * @param firewallRules the rules which should be added to this firewall. The order rules in the parameter value matters. The rules
   *                      given earlier will have higher priority.
   *
   *                      They will be evaluated after the rules added earlier.
   */
  public void addRules(FirewallRule... firewallRules) {
    for (FirewallRule rule : firewallRules) {
      addRule(rule);
    }

  }

  /**
   *
   * @return true if this config is dirty. Otherwise returns false.
   */
  public boolean isDirty() {
    return dirty;
  }

  /**
   * Change the value of dirty flag at runtime. Allows to disable firewall for some time.
   *
   * @param dirty
   */
  public void setDirty(boolean dirty) {
    this.dirty = dirty;
  }

  /**
   * Returns view of all rules of this firewall.
   *
   * @return a list with all the rules. List is not modifiable. If you want to dynamically add rules you need to do this via appropriate
   *  methods.
   */
  public List<FirewallRule> getFirewallRules() {
    return unmodifiableList(rules);
  }

  /**
   * Removes all the rules from chain. Does not affect the threads which alrady started processing the rules before call to
   * clearRules was made.
   */
  public void clearRules() {
    rules = new ArrayList<FirewallRule>();
  }

  /**
   * Defines the firewall policy.
   *
   * @param defaultPolicyAction the general firewall policy - action should be applied to request if none of the rules matched the request.
   */
  public void setDefaultPolicyAction(FirewallAction defaultPolicyAction) {
    this.defaultPolicyAction = defaultPolicyAction;
  }

  /**
   * Returns the currently set default firewall policy/action
   *
   * @return an instance of {@link FirewallAction} defining the current policy.
   */
  public FirewallAction getDefaultPolicyAction() {
    return defaultPolicyAction;
  }

  /**
   * Meothod is experimental and currently does not do anything. It might be removed in next release.
   *
   * @return currently set default wildcard which can be used by matchers.
   */
  public String getMatchersWildcard() {
    return matchersWildcard;
  }

  /**
   * Method is experimental and currently does not do anything.It might be removed in next release.
   *
   * @return currently set default wildcard which can be used by matchers.
   */
  public boolean hasMatchersWildcardDefined() {
    return matchersWildcard != null;
  }

  /**
   * Method is experimental and currently does not do anything.It might be removed in next release.
   *
   * @param matchersWildcard new matchers wildcard
   */
  public void setMatchersWildcard(String matchersWildcard) {
    this.matchersWildcard = matchersWildcard;
  }

  /**
   * Sets the name for the request marks attribute.
   * @see net.secodo.besecurefirewall.action.impl.AcknowledgeFirewallAction
   * @see net.secodo.besecurefirewall.servlet.requestmark.RequestMarks
   *
   * @param requestMarksAttributeName the name of the request attribute under which the marks will be available
   */
  public void setRequestMarksAttributeName(String requestMarksAttributeName) {
    this.requestMarksAttributeName = requestMarksAttributeName;
  }

  /**
   * Returns request attribute name under which request marks are available. It equals to
   * {@link BeSecureFirewallConfig#DEFAULT_MARKS_REQUEST_ATTRIBUTE_NAME} unless overridden.
   *
   * @see net.secodo.besecurefirewall.action.impl.AcknowledgeFirewallAction
   * @see net.secodo.besecurefirewall.servlet.requestmark.RequestMarks
   *
   * @return the name of the request attribute under which the marks will be available
   */
  public String getRequestMarksAttributeName() {
    return requestMarksAttributeName;
  }

  public RuleMatchersRepository getMatchersRepository() {
    return matchersRepository;
  }

  public FirewallCallbacksRepository getCallbacksRepository() {
    return callbacksRepository;
  }

  public DetectionPatternParsersRepository getDetectionPatternParsersRepository() {
    return detectionPatternParsersRepository;
  }

  public FirewallActionsRepository getActionsRepository() {
    return actionsRepository;
  }
}
