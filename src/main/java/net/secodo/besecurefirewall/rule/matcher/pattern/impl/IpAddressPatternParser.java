package net.secodo.besecurefirewall.rule.matcher.pattern.impl;

import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPatternParser;
import java.util.StringTokenizer;


public class IpAddressPatternParser implements DetectionPatternParser<String[]> {
  private static final String PATTERN_DELIMITER = ",";
  private String[] ips;

  public String getConfigSignature() {
    return "IP_ADDRESS_PATTERN_PARSER";
  }

  @Override
  public IpAddressPattern parse(String pattern) {
    StringTokenizer tokenizer = new StringTokenizer(pattern, PATTERN_DELIMITER);

    ips = new String[tokenizer.countTokens()];

    int index = 0;

    while (tokenizer.hasMoreTokens()) {
      ips[index++] = tokenizer.nextToken();
    }

    return new IpAddressPattern(pattern, ips);

  }

}
