package net.secodo.besecurefirewall.rule.matcher.pattern;

import net.secodo.besecurefirewall.rule.matcher.RuleMatcher;


/**
 * Represents a pattern used by instance of {@link RuleMatcher}
 * to decide whether current request matches the pattern or not.
 *
 * @param <T> indicates the return type of {@link #getParsedPattern()} method, which is the type of pattern parsed by
 *           {@link DetectionPatternParser}
 */
public interface DetectionPattern<T> {
  /**
   * Returns the pattern. The pattern could have been parsed by instance of {@link DetectionPatternParser} if parser
   * of pattern was defined. If parase was not defined than this equals to {@link #getPatternString()}
   *
   * @return pattern parsed by provided {@link DetectionPatternParser} or original pattern string if no parser was provided.
   */
  T getParsedPattern();

  /**
   * Returns the pattern string as defined in config file
   *
   * @return the raw pattern string (as defined in config)
   */
  String getPatternString();

}
