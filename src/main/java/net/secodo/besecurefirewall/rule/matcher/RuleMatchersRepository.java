package net.secodo.besecurefirewall.rule.matcher;

import net.secodo.besecurefirewall.rule.matcher.impl.IpAddressMatcher;
import net.secodo.besecurefirewall.rule.matcher.impl.ServletPathMatcher;
import net.secodo.besecurefirewall.rule.matcher.impl.UrlMatcher;
import java.util.HashMap;
import java.util.Map;


/**
 * Stores and provides all available rule matchers of the firewall.
 */
public class RuleMatchersRepository {
  private Map<String, RuleMatcher> registeredMatchers = new HashMap<String, RuleMatcher>();


  public RuleMatchersRepository() {
    final ServletPathMatcher pathMatcher = new ServletPathMatcher();
    final UrlMatcher urlMatcher = new UrlMatcher();
    final IpAddressMatcher ipAddressMatcher = new IpAddressMatcher();

    registeredMatchers.put(pathMatcher.getConfigSignature(), pathMatcher);
    registeredMatchers.put(urlMatcher.getConfigSignature(), urlMatcher);
    registeredMatchers.put(ipAddressMatcher.getConfigSignature(), ipAddressMatcher);
  }

  /**
   * Allows to register rule matcher. Given rule matcher will be registered in this repository with
   * configSignature as defined by given configSignature
   *
   * @param configSignature the identifier of matcher to register
   * @param ruleMatcher the rule matcher to register in this instance of repository
   * @return this instance of repository
   */
  public RuleMatchersRepository registerMatcher(String configSignature, RuleMatcher ruleMatcher) {
    registeredMatchers.put(configSignature, ruleMatcher);
    return this;
  }

  /**
   * Returns an instance of {@link RuleMatcher} registered in this repository with given config signature
   *
   * @see #registerMatcher(String, RuleMatcher)
   * @param configSignature the identifier of the matcher used in config file
   * @return an instance of {@link RuleMatcher} or null if there is no rule matcher registered with given configSignature
   */
  public RuleMatcher getByConfigSignature(String configSignature) {
    return registeredMatchers.get(configSignature);
  }
}
