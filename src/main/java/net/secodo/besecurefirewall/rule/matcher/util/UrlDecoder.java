package net.secodo.besecurefirewall.rule.matcher.util;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UrlDecoder {
  private static final Logger logger = Logger.getLogger(UrlDecoder.class.getSimpleName());
  private static final char PERCENT_CHARACTER = '%';


  public static String decodePercentEncodedUrl(String percentEncodedUrl) {
    if (percentEncodedUrl.indexOf(PERCENT_CHARACTER) < 0) {
      return percentEncodedUrl;
    }


    StringBuilder decodedChars = new StringBuilder();

    int charPosition = 0;
    int percentPosition = percentEncodedUrl.indexOf(PERCENT_CHARACTER, charPosition);

    decodedChars.append(percentEncodedUrl.substring(0, percentPosition));

    // keep arrays in outside the loop to prevent initialization each time
    byte[] fourBytesArray = new byte[4];
    byte[] threeBytesArray = new byte[3];
    byte[] twoBytesArray = new byte[2];
    byte[] oneByteArray = new byte[1];

    byte[] byteCharsArray = oneByteArray;


    while (percentPosition >= 0) {
      int numberOfDecodedChars = 0;

      if (percentPosition <= (percentEncodedUrl.length() - 3)) { // the sequence consists of 3 chars, for example: %AE

        int maxUtf8SequenceLength = determineMaxValidUtf8Sequence(percentEncodedUrl, percentPosition, fourBytesArray);

        // at least 3 character are needed for decoding for example: %AE or %C3%BC

        if (maxUtf8SequenceLength > 0) {
          try {
            final String decodedChar = new String(fourBytesArray, 0, maxUtf8SequenceLength, "UTF-8");
            decodedChars.append(decodedChar);

            numberOfDecodedChars = 3 * maxUtf8SequenceLength;

          } catch (UnsupportedEncodingException e) {
            logger.log(Level.WARNING, "Unsupported encoding while decoding percent encoded url", e);
            break; // no use to analyze the string further if UTF-8 encoding is not supported
          }
        }


      }

      int nextPercentPosition = percentEncodedUrl.indexOf(PERCENT_CHARACTER,
        percentPosition + ((numberOfDecodedChars != 0) ? numberOfDecodedChars : 1));

      if (nextPercentPosition > 0) {
        decodedChars.append(percentEncodedUrl.substring(percentPosition + numberOfDecodedChars, nextPercentPosition));
      } else {
        // no more percents append the rest of the url
        decodedChars.append(percentEncodedUrl.substring(percentPosition + numberOfDecodedChars)); // TODO: this is not needed because the

        // position is already known
      }

      percentPosition = nextPercentPosition;

    }


    return decodedChars.toString();
  }

  /**
   * Determines max valid utf-8 sequence withing percent encoded url, places decoded bytes in byte array passed as an argument. Returns the
   * number of bytes which are valid.
   *
   * This means that exactly returned number of bytes should be considered from passed bytes array
   *
   * @param percentEncodedUrl
   * @param firstPercentSignPosition
   * @param fourByteArray
   * @return 0 if no valid bytes are available. Otherwise returns number of valid bytes.
   */
  private static int determineMaxValidUtf8Sequence(final String percentEncodedUrl, final int firstPercentSignPosition,
                                                   final byte[] fourByteArray) {
    int percentPosition = firstPercentSignPosition;

    String utfByteChars = percentEncodedUrl.substring(percentPosition + 1, percentPosition + 3);
    final int parsedFirstByte;

    try {
      parsedFirstByte = Integer.parseInt(utfByteChars, 16);
    } catch (NumberFormatException e) {
      // unable to parse first byte.. something is wrong - byte is not valid.. returning not valid sequence
      return 0;
    }

    byte firstByte = (byte) parsedFirstByte;

    final int expectedUtf8SequenceLength = getExpectedUtf8SequenceLength(firstByte);

    if (expectedUtf8SequenceLength == 0) {
      return 0; // no valid bytes at this position

    }

    // now determine if there is valid number of percent signs for decoding
    for (int i = 1; i < expectedUtf8SequenceLength; i++) { // 1 because the 0-th byte was already verified

      int expectedPercentSignPosition = firstPercentSignPosition + (3 * i); // %AA%BB%CC - each percent sign on every 3rd position

      if (expectedPercentSignPosition > percentEncodedUrl.length()) { // string not long enough
        return 0; // no valid bytes at this position
      }

      if (percentEncodedUrl.charAt(expectedPercentSignPosition) != PERCENT_CHARACTER) {
        return 0; // not enough valid bytes
      }

    }

    int byteArrayIndex = 0;
    fourByteArray[byteArrayIndex++] = firstByte;


    percentPosition = firstPercentSignPosition + 3;

    // when we know there is enough bytes we can start decoding them from percent encoded string
    for (int i = 1; i < expectedUtf8SequenceLength; i++) {
      utfByteChars = percentEncodedUrl.substring(percentPosition + 1, percentPosition + 3);

      final int currentByte;
      try {
        currentByte = Integer.parseInt(utfByteChars, 16);
      } catch (NumberFormatException e) {
        // unable to parse the byte.. something is wrong - byte is not valid.. returning not valid sequence
        return 0;
      }

      fourByteArray[byteArrayIndex++] = (byte) currentByte;

      percentPosition += (3 * i);
    }

    if (isUTF8(fourByteArray, expectedUtf8SequenceLength)) {
      return expectedUtf8SequenceLength;
    }

    return 0; // bytes were not valid at this position
  }

  private static final byte b10000000 = (byte) Integer.parseInt("10000000", 2);
  private static final byte b00000000 = (byte) Integer.parseInt("00000000", 2);
  private static final byte b11000000 = (byte) Integer.parseInt("11000000", 2);
  private static final byte b11100000 = (byte) Integer.parseInt("11100000", 2);
  private static final byte b11110000 = (byte) Integer.parseInt("11110000", 2);
  private static final byte b11111000 = (byte) Integer.parseInt("11111000", 2);
  private static final byte b11111100 = (byte) Integer.parseInt("11111100", 2);
  private static final byte b11111110 = (byte) Integer.parseInt("11111110", 2);

  private static boolean isUTF8(final byte[] text, int textLength) {
    // TODO: remove assert
    assert textLength <= text.length;

    int expectedLength = 0;

    for (int i = 0; i < textLength; i++) {
      expectedLength = getExpectedUtf8SequenceLength(text[i]);

      if (expectedLength == 0) {
        return false;
      }

      while (--expectedLength > 0) {
        if (++i >= textLength) {
          return false;
        }
        if ((text[i] & b11000000) != b10000000) {
          return false;
        }
      }
    }

    return true;
  }

  private static int getExpectedUtf8SequenceLength(byte b) {
    final int expectedLength;

    if ((b & b10000000) == b00000000) {
      expectedLength = 1;
    } else if ((b & b11100000) == b11000000) {
      expectedLength = 2;
    } else if ((b & b11110000) == b11100000) {
      expectedLength = 3;
    } else if ((b & b11111000) == b11110000) {
      expectedLength = 4;
    } else if ((b & b11111100) == b11111000) {
      expectedLength = 5;
    } else if ((b & b11111110) == b11111100) {
      expectedLength = 6;
    } else {
      expectedLength = 0;
    }
    return expectedLength;
  }


}
