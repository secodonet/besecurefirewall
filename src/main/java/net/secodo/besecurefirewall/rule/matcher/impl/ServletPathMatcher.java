package net.secodo.besecurefirewall.rule.matcher.impl;

import net.secodo.besecurefirewall.rule.matcher.RuleMatcher;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;
import net.secodo.besecurefirewall.rule.matcher.util.UrlDecoder;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;


/**
 * A matcher which matches requests basing on patterns found within servlet path. A servlet path is considered to be the part of the request uri without
 * the application context under which current web application is deployed. For example if the application is deployed under "http://localhost:8080/myapp"
 * and the executed servlet/controller is under URI: "http://localhost:8080/myapp/my/controller/abc" this matcher identifies patterns only within
 * "/my/controller/abc" part of the URI.
 *
 *
 */
public class ServletPathMatcher implements RuleMatcher {
  public final String configSignature = "SERVLET_PATH_MATCHER";
  private static final Logger logger = Logger.getLogger(ServletPathMatcher.class.getSimpleName());

  public String getConfigSignature() {
    return configSignature;
  }

  @Override
  public boolean matches(ServletRequestWrapper requestWrapper, DetectionPattern<?> pattern) {
    final ServletRequest servletRequest = requestWrapper.getServletRequest();

    if (servletRequest instanceof HttpServletRequest) {
      final HttpServletRequest request = (HttpServletRequest) servletRequest;

      if ((request.getRequestURI() == null) || (request.getContextPath() == null)) {
        logger.warning(
          "Either requestUri or contextPath is null. Unable to do proper matching. Returning request as not matched. RequestURI: " +
          request.getRequestURI() +
          ", contextPath:" + request.getContextPath());
        return false;
      }

      String path = request.getRequestURI().substring(request.getContextPath().length());
      path = UrlDecoder.decodePercentEncodedUrl(path);

      final String patternString = pattern.getPatternString();

      return path.indexOf(patternString) >= 0;

    }

    return false;

  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if ((o == null) || (getClass() != o.getClass())) {
      return false;
    }

    ServletPathMatcher matcher = (ServletPathMatcher) o;

    return !((configSignature != null) ? (!configSignature.equals(matcher.configSignature))
                                       : (matcher.configSignature != null));

  }

  @Override
  public int hashCode() {
    return (configSignature != null) ? configSignature.hashCode() : 0;
  }
}
