package net.secodo.besecurefirewall.rule.matcher.pattern;

import net.secodo.besecurefirewall.rule.matcher.pattern.impl.IpAddressPatternParser;
import java.util.HashMap;
import java.util.Map;


/**
 * Stores and provides all available parsers of detection patterns.
 */
public class DetectionPatternParsersRepository {
  private Map<String, DetectionPatternParser<?>> registeredParsers = new HashMap<String, DetectionPatternParser<?>>();


  public DetectionPatternParsersRepository() {
    final IpAddressPatternParser ipAddressesDetectionPattern = new IpAddressPatternParser();

    registeredParsers.put(ipAddressesDetectionPattern.getConfigSignature(), ipAddressesDetectionPattern);
  }

  /**
   * Allows to register pattern parser. Given pattern parser will be registered in this repository with
   * given configSignature as identifier.
   *
   * @param configSignature the identifier of given patternParser which will be used to register the parser in this repository
   * @param patternParser the parser to register in this instance of repository
   * @return this instance of repository
   */
  public DetectionPatternParsersRepository registerPattern(String configSignature,
                                                           DetectionPatternParser<?> patternParser) {
    registeredParsers.put(configSignature, patternParser);

    return this;
  }

  /**
   * Returns an instance of {@link DetectionPatternParser} registered in this repository with given config signature
   *
   * @see #registerPattern(String, DetectionPatternParser)
   * @param configSignature the identifier of the detection pattern used in config file
   * @return an instance of {@link DetectionPatternParser} or null if there is no parser registered with given configSignature
   */
  public DetectionPatternParser<?> getByConfigSignature(String configSignature) {
    return registeredParsers.get(configSignature);
  }
}
