package net.secodo.besecurefirewall.rule.matcher.impl;

import net.secodo.besecurefirewall.rule.matcher.RuleMatcher;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;
import net.secodo.besecurefirewall.rule.matcher.util.UrlDecoder;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;


/**
 * A matcher which matches requests basing on patterns found within whole url (including host, port, protocol, servlet path, query string).
 */
public class UrlMatcher implements RuleMatcher {
  public final String configSignature = "URL_MATCHER";

  public String getConfigSignature() {
    return configSignature;
  }

  @Override
  public boolean matches(ServletRequestWrapper requestWrapper, DetectionPattern<?> pattern) {
    final ServletRequest servletRequest = requestWrapper.getServletRequest();

    if (servletRequest instanceof HttpServletRequest) {
      final HttpServletRequest request = (HttpServletRequest) servletRequest;
      String url = request.getRequestURL().toString();
      url = UrlDecoder.decodePercentEncodedUrl(url);

      String queryString = request.getQueryString();

      if (queryString != null) {
        queryString = UrlDecoder.decodePercentEncodedUrl(queryString);

        if (url.endsWith("?")) {
          url = url.substring(0, url.length() - 1);
        }

        url = url + "?" + queryString;
      }

      final String patternString = pattern.getPatternString();

      return url.indexOf(patternString) >= 0;

    }

    return false;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if ((o == null) || (getClass() != o.getClass())) {
      return false;
    }

    UrlMatcher that = (UrlMatcher) o;

    return !((configSignature != null) ? (!configSignature.equals(that.configSignature))
                                       : (that.configSignature != null));

  }

  @Override
  public int hashCode() {
    return (configSignature != null) ? configSignature.hashCode() : 0;
  }
}
