package net.secodo.besecurefirewall.rule.matcher.pattern;


/**
 * Can be defined to parse detection pattern string into instance of some class.
 *
 * @param <T> the type of parsed instance
 */
public interface DetectionPatternParser<T> {
  DetectionPattern<T> parse(String patternText);
}
