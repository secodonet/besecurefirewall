package net.secodo.besecurefirewall.rule.matcher.pattern.impl;

import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;
import java.util.Arrays;


public class IpAddressPattern implements DetectionPattern<String[]> {
  private final String rawPattern;
  private final String[] parsedIps;

  public IpAddressPattern(String rawPattern, String[] parsedIps) {
    this.rawPattern = rawPattern;

    this.parsedIps = parsedIps;
  }

  @Override
  public String[] getParsedPattern() {
    return parsedIps;
  }

  @Override
  public String getPatternString() {
    return null;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if ((o == null) || (getClass() != o.getClass())) {
      return false;
    }

    IpAddressPattern that = (IpAddressPattern) o;

    if ((rawPattern != null) ? (!rawPattern.equals(that.rawPattern)) : (that.rawPattern != null)) {
      return false;
    }

    // Probably incorrect - comparing Object[] arrays with Arrays.equals
    return Arrays.equals(parsedIps, that.parsedIps);

  }

  @Override
  public int hashCode() {
    int result = (rawPattern != null) ? rawPattern.hashCode() : 0;
    result = (31 * result) + Arrays.hashCode(parsedIps);
    return result;
  }

  @Override
  public String toString() {
    return "IpAddressPattern{" +
      "parsedIps=" + Arrays.toString(parsedIps) +
      '}';
  }
}
