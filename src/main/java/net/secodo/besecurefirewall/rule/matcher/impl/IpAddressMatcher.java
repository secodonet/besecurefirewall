package net.secodo.besecurefirewall.rule.matcher.impl;

import net.secodo.besecurefirewall.rule.matcher.RuleMatcher;
import net.secodo.besecurefirewall.rule.matcher.pattern.impl.IpAddressPattern;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;


/**
 * A matcher which matches requests with given IP addresses (by examining {@link HttpServletRequest#getRemoteAddr()}
 */
public class IpAddressMatcher implements RuleMatcher {
  private final String configSignature = "IP_ADDRESS_MATCHER";

  public String getConfigSignature() {
    return configSignature;
  }

  @Override
  public boolean matches(final ServletRequestWrapper requestWrapper, final DetectionPattern<?> pattern) {
    final ServletRequest servletRequest = requestWrapper.getServletRequest();

    final String clientIp = servletRequest.getRemoteAddr();

    if (pattern instanceof IpAddressPattern) {
      for (String ip : ((IpAddressPattern) pattern).getParsedPattern()) {
        if (ip.equals(clientIp)) {
          return true;
        }
      }

      return false;
    } else {
      return pattern.getPatternString().equals(clientIp);
    }


  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if ((o == null) || (getClass() != o.getClass())) {
      return false;
    }

    IpAddressMatcher that = (IpAddressMatcher) o;

    return !((configSignature != null) ? (!configSignature.equals(that.configSignature))
                                       : (that.configSignature != null));

  }

  @Override
  public int hashCode() {
    return (configSignature != null) ? configSignature.hashCode() : 0;
  }
}
