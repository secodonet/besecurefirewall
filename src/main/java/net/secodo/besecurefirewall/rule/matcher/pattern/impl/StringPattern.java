package net.secodo.besecurefirewall.rule.matcher.pattern.impl;

import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;


public class StringPattern implements DetectionPattern<String> {
  private final String pattern;

  public StringPattern(String pattern) {
    this.pattern = pattern;
  }

  @Override
  public String getParsedPattern() {
    return pattern;
  }

  @Override
  public String getPatternString() {
    return pattern;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if ((o == null) || (getClass() != o.getClass())) {
      return false;
    }

    StringPattern that = (StringPattern) o;

    return !((pattern != null) ? (!pattern.equals(that.pattern)) : (that.pattern != null));

  }

  @Override
  public int hashCode() {
    return (pattern != null) ? pattern.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "StringPattern{" +
      "pattern='" + pattern + '\'' +
      '}';
  }
}
