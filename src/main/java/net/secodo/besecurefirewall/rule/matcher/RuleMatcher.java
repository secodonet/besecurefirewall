package net.secodo.besecurefirewall.rule.matcher;

import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;


/**
 * Represents the matcher, which determines whether certain {@link FirewallAction}
 * should be applied when processing current request.
 */
public interface RuleMatcher {
  /**
   * Determines whather rule containing this rule matcher should handle the request
   *
   * @param requestWrapper the actual servlet request which will be passed to request processing
   * @param pattern the detection pattern a which will be examined by this rule matcher
   * @return true if this rule matcher matches the request
   */
  boolean matches(final ServletRequestWrapper requestWrapper, final DetectionPattern<?> pattern);

}
