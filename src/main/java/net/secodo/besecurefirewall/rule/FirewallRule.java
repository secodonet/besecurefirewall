package net.secodo.besecurefirewall.rule;

import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.rule.matcher.RuleMatcher;
import net.secodo.besecurefirewall.rule.callback.FirewallRuleCallback;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Represents the whole firewall rule which consists of:
 *
 * <ul>
 *   <li>name - name of the rule - in general it should be unique string identifying the rule</li>
 *   <li>rule matcher - which determines whether this rule should apply to current request</li>
 *   <li>detection pattern - used by rule matcher to check whether current request matches the pattern</li>
 *   <li>onMatchCallbacks (optional) - contains callback functions which will be triggered after request matched
 *   the pattern, callbacks are executed in order and before the firewall action handles the request</li>
 *   <li>firewall action - defines what should happen when this rule matched the request (allow, deny etc.), action can execute the actual request in the FilterChain</li>
 * </ul>
 *
 * To build a rule in a fluent-interface manner {@link net.secodo.besecurefirewall.rule.buillder.RuleBuilder}
 */
public class FirewallRule {
  private final String name;
  private final RuleMatcher matcher;
  private final List<FirewallRuleCallback> onMatchCallbacks;
  private final DetectionPattern<?> pattern;
  private final FirewallAction action;

  public FirewallRule(String name, RuleMatcher matcher, List<? extends FirewallRuleCallback> onMatchCallbacks,
                      DetectionPattern<?> pattern, FirewallAction action) {
    this.name = name;
    this.matcher = matcher;
    this.onMatchCallbacks = Collections.unmodifiableList(new ArrayList<FirewallRuleCallback>(onMatchCallbacks));
    this.pattern = pattern;
    this.action = action;
  }

  public FirewallRule(String name, RuleMatcher matcher, DetectionPattern<?> pattern, FirewallAction action) {
    this(name, matcher, Collections.<FirewallRuleCallback>emptyList(), pattern, action);
  }

  public List<FirewallRuleCallback> getOnMatchCallbacks() {
    return onMatchCallbacks;
  }

  public String getName() {
    return name;
  }

  public RuleMatcher getMatcher() {
    return matcher;
  }

  public FirewallAction getAction() {
    return action;
  }

  /**
   * Returns {@link DetectionPattern} detected by this rule.
   *
   * @param <T> the type of the parsed pattern. Usually {@link String}.
   * @return pattern detected by this rule
   */
  @SuppressWarnings("unchecked")
  public <T> DetectionPattern<T> getDetectionPattern() {
    return (DetectionPattern<T>) pattern;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    FirewallRule that = (FirewallRule) o;

    if (name != null ? !name.equals(that.name) : that.name != null) return false;
    if (matcher != null ? !matcher.equals(that.matcher) : that.matcher != null) return false;
    if (onMatchCallbacks != null ? !onMatchCallbacks.equals(that.onMatchCallbacks) : that.onMatchCallbacks != null)
      return false;
    if (pattern != null ? !pattern.equals(that.pattern) : that.pattern != null) return false;
    return action != null ? action.equals(that.action) : that.action == null;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (matcher != null ? matcher.hashCode() : 0);
    result = 31 * result + (onMatchCallbacks != null ? onMatchCallbacks.hashCode() : 0);
    result = 31 * result + (pattern != null ? pattern.hashCode() : 0);
    result = 31 * result + (action != null ? action.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "FirewallRule{" +
      "name='" + name + '\'' +
      ", matcher=" + matcher +
      ", onMatchCallbacks=" + onMatchCallbacks +
      ", pattern=" + pattern +
      ", action=" + action +
      '}';
  }
}
