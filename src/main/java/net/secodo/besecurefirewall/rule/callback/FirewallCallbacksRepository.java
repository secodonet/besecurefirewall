package net.secodo.besecurefirewall.rule.callback;

import net.secodo.besecurefirewall.rule.callback.impl.MarkMatchedRequestCallback;

import java.util.HashMap;
import java.util.Map;


/**
 * Stores and provides all available parsers of detection patterns.
 */
public class FirewallCallbacksRepository {
  private Map<String, FirewallRuleCallback> registeredCallbacks = new HashMap<String, FirewallRuleCallback>();

  public interface Signatures {
    String MARK_REQUEST = "MARK_MATCHED_REQUEST";
  }

  public FirewallCallbacksRepository() {
    // register default callbacks
    final MarkMatchedRequestCallback markMatchedRequestCallback = new MarkMatchedRequestCallback();

    registeredCallbacks.put(Signatures.MARK_REQUEST, markMatchedRequestCallback);

  }

  /**
   * Allows to manually register firewall rule callback firewall method.
   *
   * @param configSignature the signature under which given callback will be reachable within this repository
   * @param ruleCallback    the firewall callback method to register in this instance of repository
   * @return this instance of repository
   */
  public FirewallCallbacksRepository registerCallback(String configSignature, FirewallRuleCallback ruleCallback) {
    registeredCallbacks.put(configSignature, ruleCallback);

    return this;
  }

  /**
   * Returns null if there is no callback function registered with given config signature
   * @param configSignature the identifier of the callback function used in config file
   * @return
   */
  /**
   * Returns an instance of {@link FirewallRuleCallback} registered in this repository with given config signature
   *
   * @param configSignature the identifier of the callback used in config file
   * @return an instance of {@link FirewallRuleCallback} or null if there is no callback registered with given configSignature
   * @see #registerCallback(String, FirewallRuleCallback)
   */
  public FirewallRuleCallback getByConfigSignature(String configSignature) {
    return registeredCallbacks.get(configSignature);
  }


}
