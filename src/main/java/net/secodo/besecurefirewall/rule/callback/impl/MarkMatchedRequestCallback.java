package net.secodo.besecurefirewall.rule.callback.impl;

import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.logging.FirewallLogger;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.callback.FirewallRuleCallback;
import net.secodo.besecurefirewall.servlet.requestmark.impl.RuleBasedMatchedMark;
import net.secodo.besecurefirewall.servlet.requestmark.RequestMarks;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;

public class MarkMatchedRequestCallback implements FirewallRuleCallback {
  private final FirewallLogger logger;

  public MarkMatchedRequestCallback() {
    logger = FirewallLogger.getLogger();
  }


  @Override
  public void call(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper, FirewallRule rule) {
    final String requestMarksAttributeName = config.getRequestMarksAttributeName();

    final Object retrievedAttribute = requestWrapper.getServletRequest().getAttribute(requestMarksAttributeName);
    if (retrievedAttribute != null && RequestMarks.class.isInstance(retrievedAttribute)) {
      logger.warn("Request attribute with name: " + requestMarksAttributeName + " is not instance of: " +
        RequestMarks.class.getName() + ". Request will not be marked, although rule matched!");
    } else {
      if (retrievedAttribute == null) {
        requestWrapper.getServletRequest().setAttribute(requestMarksAttributeName, new RequestMarks());
      }
      RequestMarks requestMarks = (RequestMarks) requestWrapper.getServletRequest().getAttribute
        (requestMarksAttributeName);

      requestMarks.addMark(new RuleBasedMatchedMark(rule));
    }

  }

}
