package net.secodo.besecurefirewall.rule.callback;

import net.secodo.besecurefirewall.config.BeSecureFirewallConfig;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.servlet.wrapper.ServletRequestWrapper;


public interface FirewallRuleCallback {
  void call(BeSecureFirewallConfig config, ServletRequestWrapper requestWrapper, FirewallRule rule);

}
