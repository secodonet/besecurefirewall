package net.secodo.besecurefirewall.rule.buillder;

import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPatternParser;


public class RuleBuilderException extends Exception {
  private static final long serialVersionUID = 6739594322255680599L;

  private final String ruleName;
  private final String pattern;
  private final DetectionPatternParser<?> parser;


  public RuleBuilderException(String ruleName, String pattern, DetectionPatternParser<?> parser, String message) {
    super(message);
    this.ruleName = ruleName;
    this.pattern = pattern;
    this.parser = parser;
  }

  public String getRuleName() {
    return ruleName;
  }

  public String getPattern() {
    return pattern;
  }

  public DetectionPatternParser<?> getParser() {
    return parser;
  }
}
