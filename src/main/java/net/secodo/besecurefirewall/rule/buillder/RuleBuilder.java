package net.secodo.besecurefirewall.rule.buillder;

import net.secodo.besecurefirewall.action.FirewallAction;
import net.secodo.besecurefirewall.rule.callback.FirewallRuleCallback;
import net.secodo.besecurefirewall.rule.matcher.RuleMatcher;
import net.secodo.besecurefirewall.rule.FirewallRule;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPattern;
import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPatternParser;
import net.secodo.besecurefirewall.rule.matcher.pattern.impl.StringPattern;

import java.util.ArrayList;
import java.util.List;


/**
 * A builder of the rules. Can be used to create an instance of {@link FirewallRule} using fluent interface and builder
 * pattern.
 *
 */
public class RuleBuilder {
  private String name;
  private RuleMatcher matcher;
  private List<FirewallRuleCallback> onMatchCallbacks = new ArrayList<FirewallRuleCallback>();
  private FirewallAction action;
  private String pattern;
  private DetectionPatternParser<?> patternParser;

  private RuleBuilder() {
  }

  public static RuleBuilder init() {
    return new RuleBuilder();
  }

  public RuleBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public RuleBuilder withMatcher(RuleMatcher matcher) {
    this.matcher = matcher;
    return this;
  }

  public RuleBuilder withOnMatchCallback(FirewallRuleCallback onMatchCallback) {
    this.onMatchCallbacks.add(onMatchCallback);
    return this;
  }

  public RuleBuilder withAction(FirewallAction action) {
    this.action = action;
    return this;
  }

  public RuleBuilder withPattern(String pattern) {
    this.pattern = pattern;
    return this;
  }

  public RuleBuilder withDetectionPatternParser(DetectionPatternParser<?> patternParser) {
    this.patternParser = patternParser;
    return this;
  }

  public FirewallRule build() throws RuleBuilderException {
    verify();

    final DetectionPattern<?> parsedPattern;
    if (patternParser != null) {
      try {
        parsedPattern = patternParser.parse(pattern);

      } catch (Exception e) {
        throw new RuleParsingException(name, pattern, patternParser);
      }
    } else {
      parsedPattern = new StringPattern(pattern);
    }

    return new FirewallRule(name, matcher, onMatchCallbacks, parsedPattern, action);
  }


  private void verify() throws IncompleteRuleException {
    if (name == null) {
      throw new IncompleteRuleException(name, pattern, patternParser, "name");
    }
    if (matcher == null) {
      throw new IncompleteRuleException(name, pattern, patternParser, "matcher");
    }
    if (action == null) {
      throw new IncompleteRuleException(name, pattern, patternParser, "action");
    }
    if (pattern == null) {
      throw new IncompleteRuleException(name, pattern, patternParser, "pattern");
    }
  }


}
