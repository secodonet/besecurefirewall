package net.secodo.besecurefirewall.rule.buillder;

import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPatternParser;


/**
 * Thrown when there wa a problem with parsing the rule. For example when {@link DetectionPatternParser} was defined but it
 * could not parse the pattern.
 *
 */
public class RuleParsingException extends RuleBuilderException {
  private static final long serialVersionUID = 5741786716032398224L;

  public RuleParsingException(String ruleName, String pattern, DetectionPatternParser<?> parser) {
    super(ruleName,
      pattern,
      parser,
      "Unable to parse the pattern: " + pattern + " with parser: " +
      ((parser != null) ? parser.getClass() : "") + " for rule: " + ruleName);
  }

}
