package net.secodo.besecurefirewall.rule.buillder;

import net.secodo.besecurefirewall.rule.matcher.pattern.DetectionPatternParser;


/**
 * Thrown by config parser when part of rule definition is missing in config file.
 */
public class IncompleteRuleException extends RuleBuilderException {
  private static final long serialVersionUID = 7262371471428959984L;
  private final String missingField;

  public IncompleteRuleException(String ruleName, String pattern, DetectionPatternParser<?> parser,
                                 String missingField) {
    super(ruleName,
      pattern,
      parser,
      "Field definition incomplete" + ((ruleName != null) ? (" for rule: " + ruleName) : "") + ". Field name: " +
      missingField);
    this.missingField = missingField;
  }

  public String getMissingField() {
    return missingField;
  }
}
