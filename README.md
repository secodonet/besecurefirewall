# About #

BeSecureFirewall provides HTTP firewall for your java applications. BeSecureFirewall is licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0).


```
#!

Copyright 2017 secodo.net 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use any file in this project except in compliance 
with the License. You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```


# Documentation #

Below you can find instruction how set up the firewall as well as how to configure firewall rules.

## Changelog ##

### v1.0-RC3
- added AcknowledgeFirewallAction (identified by ACKNOWLEDGE signature) - allows to acknowledge that the request was 
matched by a rule but does not handle the request. Allows to execute custom callbacks when request matched.
- added possibility to add more than one onMatchCallback (callbacks are comma separated)
- BeSecureFirewallConfig is passed as an argument of FirewallCallback and FirewallAction
- ServletRequestWrapper and ServletResponseWrapper changed packages to net.secodo.besecurefirewall.servlet.wrapper
 (NOTE: this should not have any impack on applications using the firewall unless custom Action was defined - in this
  case the import should be fixed)

## Installing firewall into your application ##

Firewall acts as a HTTP Filter](http://docs.oracle.com/cd/E17802_01/products/products/servlet/2.5/docs/servlet-2_5-mr2/) therefore installation is done the same way as for any HTTP filter and can be done either declaratively inside deployment descriptor (web.xml) file or programmatically via servlet api. The filter class which needs to be installed is: BeSecureFirewall (TODO: add api doc link). The filter acts as HTTP firewall and needs to be initialized with configuration which can either be defined in configuration file (properties file) or built manually in application code.

### Declarative installation via web.xml ###

TODO:

### Programatic installation via servlet api ###

The web firewall can be installed using servlet api 3.0 capabilities. Bellows example assumes that the firewall configuration is read from "besecurefirewall.properties" configuration file. 


```
#!java

@WebListener
public class BeSecureFirewallInitializer implements ServletContextListener {
  @Override
  public void contextInitialized(ServletContextEvent sce) {
    ServletContext servletContext = sce.getServletContext();

    final BeSecureFirewall beSecureFirewall = new BeSecureFirewall();
    final FilterRegistration.Dynamic filterRegistration = servletContext.addFilter(
      BeSecureFirewall.class.getSimpleName(),
      beSecureFirewall);
    filterRegistration.setInitParameter("configfile", "besecurefirewall.properties");
    filterRegistration.addMappingForUrlPatterns(null, true, "/*");

  }

  @Override
  public void contextDestroyed(ServletContextEvent servletContextEvent) {
  }
}

```



### Programatic installation in spring application ###

Since Spring Framework provides direct access to servlet api via WebApplicatinInitializer interface, you can bring up BeSecureFirewall to life directly in Spring. In belows example we manually set up the firewall filter and pass a configuration file besecurefirewall.properties as an init parameter. Bellows example assumes that the firewall configuration is read from "besecurefirewall.properties" configuration file. 


```
#!java

public class AppInitializer implements WebApplicationInitializer {
  // (...)

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {

	// (...) initialize the application, including spring dispatcher servlet and other filters

    addBeSecureFirewallFilter(servletContext);
  }
  
  private void addBeSecureFirewallFilter(ServletContext servletContext) {
    final BeSecureFirewall beSecureFirewall = new BeSecureFirewall();
    final FilterRegistration.Dynamic filterRegistration = servletContext.addFilter(
      BeSecureFirewall.class.getSimpleName(),
      beSecureFirewall);
    filterRegistration.setInitParameter("configfile", "besecurefirewall.properties");
    filterRegistration.addMappingForUrlPatterns(null, true, "/*");

  }
}
```

## Configuration file and options ##

### Firewall policy ###

Main option to set is the firewall policy. The policy defines what should happen in case firewall rules did not not process the request (there was no rule which matched the request). The policy is represented by a FirewallAction which should be executed when a request was not processed by any rule.

Following policies are supported (but effectively you can use any FirewallAction to represent policy):

* ALLOW - allows the request to go to application
* DENY - request will be blocked with HTTP 403 Forbidden response header

In configuration file the firewall policy is set via *policy.default.action* config file. For example to allow all requests which did not match any rule to be passed to the application you write:


```
#!properties
policy.default.action = ALLOW

```





# Contribution

## Developer set-up ##

* No special set-up is needed. You need to either rely on unit tests or install the firewall to the working application using instruction above.

## Contribution guidelines ##

* Test coverage of added core functionality is required
* Code review by one of the owners

## Who do I talk to? ##

* Repository is currently owned by secodo.net (info@secodo.net)
* actively developed by Andrzej Martynowicz (andrzej.martynowicz@gmail.com)

# Thanks #
Many thanks for support for the project to ImmobilienScout24 (https://www.immobilienscout24.de/) and to Ravi Yadav for his valuable input on this and other project.